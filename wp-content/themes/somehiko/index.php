<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Somehito Nikibi | Somehiko ○ Nikibi | 東京のWEB制作会社で働く男の日記</title>
	<meta name="description" content="東京のWEB制作会社で働く男の日記">

	<link rel="alternate" type="application/rss+xml" title="Somehiko ○ Nikibi RSS Feed"
		  href="http://somehitonikibi.boy.jp/feed">
	<link rel="alternate" type="application/atom+xml" title="Somehiko ○ Nikibi Atom Feed"
		  href="http://somehitonikibi.boy.jp/feed/atom">
	<link rel="pingback" href="http://somehitonikibi.boy.jp/xmlrpc.php">


	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/japanese.css" type="text/css">

	<meta property="og:title" content="">
	<meta property="og:site_name" content="Somehiko ○ Nikibi">
	<meta property="og:url" content="http://somehitonikibi.boy.jp/archives/859">
	<meta property="og:type" content="article">
	<meta property="fb:app_id" content="203660986434625">
	<!-- HeadSpace SEO 3.6.41 by John Godley - urbangiraffe.com -->
	<meta name="description" content="手染めをする染めヒトの、中学時代に出来たおでこのニキビのようなBlogです。">
	<!-- HeadSpace -->

	<!-- End Newpost Catch ver1.0.9 -->
	<meta property="og:title" content="Somehiko ○ Nikibi">
	<meta property="og:site_name" content="Somehiko ○ Nikibi">
	<meta property="og:description" content="東京のWEB制作会社で働く男の日記">
	<meta property="og:type" content="blog">
	<meta property="og:url" content="http://somehiko.info">
	<meta property="fb:admins" content="497246096967370">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js?ver=1.8.3"></script>


</head>


<body class="default">
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-50650037-2', 'auto');
	ga('send', 'pageview');

</script>

<div id="header">
	<h1 id="logo"><a href="http://somehiko.info/">Somehiko ○ Nikibi</a></h1>
</div>
<!-- END #header -->


<div id="content">

<div id="main_content" class="cf">
<div id="left_col" style="margin:auto;">

<div class="banner_area">
	<a <?php ga_input(1,'top_banner_1'); ?> target="_blank" href="http://liginc.co.jp/author/somehiko"><img src="<?php bloginfo('template_directory'); ?>/images/banner1.jpg" alt="banner1" /></a>
	<a <?php ga_input(2,'top_banner_2'); ?> target="_blank" href="http://liginc.co.jp/author/shinsan"><img src="<?php bloginfo('template_directory'); ?>/images/banner2.jpg" alt="banner2" /></a>
</div>



<div class="post first_post">
<h2 class="title no_date">１ヶ月後のインタビュー</h2>

<div class="post_content cf">
<p>
</p>

<p style="text-align: center;"><strong><br>
	</strong></p>

<p style="text-align: center;"><a

		href="http://liginc.co.jp/"><img title="株式会社LIG"
										 src="<?php bloginfo('template_directory'); ?>/files/82f1b671c9423479b38dc54d8b46fe3b-300x136.png" alt=""
										 width="300" height="136"></a></p>

<p style="text-align: center;">株式会社LIGの広報担当ジェイさんの服を染めたい。<br>
	そのためには四神を倒さないといけない。<br>
	そんなところから始まったJ for Japs。<br>
	４４日目の昼が迎えようとしていた。</p>

<p style="text-align: center;"><a
		title="株式会社 LIG"
		href="http://liginc.co.jp/" target="_blank">http://liginc.co.jp/</a></p>
&nbsp;<br>

<p style="text-align: center;">&nbsp;<img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">————————————————-</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-828" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/e06905e803226d808fbd00657938713b-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;"><strong>柴犬をジェイさんに倒してもらったのをつゆしらず困り果てていたあのときから、<br>
		１ヶ月の時が経ち、日付は１２月１４日となっていた。</strong></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">————————————————–</p>

<p style="text-align: center;"><img class="aligncenter size-full wp-image-873"
									title="Japs"
									src="<?php bloginfo('template_directory'); ?>/files/d8cebd7d8bf4d6881b28f643c726da42.png"
									alt="" width="384" height="207"><br
		class="aligncenter size-full wp-image-873" title="Japs"><strong><br>
		木枯らしが少し吹き荒れる昼過ぎ。</strong><br>
	<strong>とある京都の小さな手染め工房にて、</strong><br>
	<strong>染めヒトは記者からのインタビューを受けていた。</strong></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-863" title="インタビュー"
		src="<?php bloginfo('template_directory'); ?>/files/61424bd25b8eec21b2c82dabbd5220c0-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">記者：よろしくね、今回は有難う。</p>

<p style="text-align: center;">染めヒト：いやー、インタビュー２回目ですが緊張しますね。<a

		title="スペシャルインタビュー" href="http://ai-for-japs.boy.jp/somehito/?page_id=130">前回はマネキン相手</a>だったので。今日は宜しくお願いします。
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-864" title="インタビュー"
		src="<?php bloginfo('template_directory'); ?>/files/edad1accfaf8ef1027f7a3c39910a495-300x200.png" alt="" width="300" height="200"><br>
	記者：今回はJ for Japsの話を聞きたいんだ。</p>

<p style="text-align: center;">染めヒト：わかりました。そうですね、J for Japsについてお話していくと、</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-872" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/8faf60b28110e028496b7f5edca94bf1-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：忙しくもあり、時間的に厳しい日々でしたが、JさんやLIGの皆様と交流出来た最高の１５日間でしたね。そもそものきっかけは、</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img title="Jさんとのやりとり１"
									src="<?php bloginfo('template_directory'); ?>/files/37e3808047553cedb34daa9b1d7ab2a3-200x300.png" alt=""
									width="200" height="300"></p>

<p style="text-align: center;">
	染めヒト：うちの従業員であるかおるさんが株式会社LIGのJさんに送ったこのひとつのTweetでした。このTweetからJさんに俺の衣類を染める前にまず京都の守護神である四神を倒せって言われて、そこから全てが始まりました。</p>

<p style="text-align: center;">残り１４日　J for Japs
	の全てはここから始まった</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="サイン" src="<?php bloginfo('template_directory'); ?>/files/c8a95b0e59ee6afe1a26aec384592a08-300x200.png" alt="" width="300" height="200">
</p>

<p style="text-align: center;">でも四神って京都の守護神なんで倒したらまずいよねってなって。そしたら素敵なカフェ&amp;バーのマスターが倒すというのは、四神に守られなくても大丈夫な京都に君がして、四神をいなくなくならせることなんだって教えてくれたんです。<br>
	<br style="text-align: center;"> 残り１３日　モダンタイムズで緊急会議
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="染めヒト動き４" src="<?php bloginfo('template_directory'); ?>/files/fe91a4d414056a5ebbf44b1ae3f16618-300x197.png" alt="" width="300"
		height="197"></p>

<p style="text-align: center;">
	よしじゃあ四神のいる上賀茂神社、松尾大社、城南宮、八坂神社に出向いて困った人を助けて、四神いなくても大丈夫ってところをアピールしようってなったんです。もちろん染めヒトですから、手染めで困った人を助けないといけないわけですが。<br>
	<br style="text-align: center;"> 残り１２日　これからの動きを決めるのだ
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-861" title="記者"
		src="<?php bloginfo('template_directory'); ?>/files/f6b30ef9b48ec403c9cd31fbd4b34374-200x300.png" alt="" width="200" height="300"></p>

<p style="text-align: center;">記者：……..。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-872" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/8faf60b28110e028496b7f5edca94bf1-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：そこからは怒濤の神社巡り、困った人を助ける日々が続きました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="桜子さん" src="<?php bloginfo('template_directory'); ?>/files/6bf7326cf7ff73c5396e065402e99230-300x200.png" alt="" width="300" height="200">
</p>

<p style="text-align: center;">まず玄武のいる上賀茂神社では、ハデな服しか買えないことに困っていた桜子さんの服に、</p>

<p style="text-align: center;"><br style="text-align: center;"> 残り１１日　【前編】上賀茂神社
	/ 玄武ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="桜子" src="<?php bloginfo('template_directory'); ?>/files/beb7c07de1fb2a7cde44458b8b7fa73e-300x197.png" alt="" width="300"
		height="197"></p>

<p style="text-align: center;">藍を吹き込むことで落ち着いた色の衣類に仕上げて、喜んでもらいました。</p>

<p style="text-align: center;"><br style="text-align: center;"> 残り１０日　【後編】上賀茂神社
	/ 玄武ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="浪人生" src="<?php bloginfo('template_directory'); ?>/files/e8c46215303d48db66e7ca281412af68-300x200.png" alt="" width="300" height="200">
</p>

<p style="text-align: center;">白虎のいる松尾大社では、お金がなくて服が買えず、いつも同じ服で困っていた浪人生の服に、<br style="text-align: center;"> <br
		style="text-align: center;"> 残り　９日　【前編】松尾大社 /
	白虎ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/8b74df1a323325fc9319de7bac6a941c-300x203.png" alt="" width="300" height="203">
</p>

<p style="text-align: center;">藍を吹き込んで新しく感じる衣類に仕上げて、喜んでもらいました。</p>

<p style="text-align: center;"><br style="text-align: center;"> 残り　８日　【後編】松尾大社
	/ 白虎ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/85fc2e82ed0beb211e3cb066d3b8bf94-300x203.png" alt="" width="300" height="203">
</p>

<p style="text-align: center;">朱雀のいる城南宮では熱で困っていた熱の人に、<br>
	<br style="text-align: center;"> 残り　７日　【前編】城南宮
	/ 朱雀ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/e2d9fe77a28c07296dd319b4c78c2bce-300x195.png" alt="" width="300" height="195">
</p>

<p style="text-align: center;">藍染めで使う植物が薬にも使われることを教え、大丈夫と言い聞かせ、プラシーボ効果で熱を冷まし、喜んでもらいました。</p>

<p style="text-align: center;"><br style="text-align: center;"> 残り　６日　【後編】城南宮
	/ 朱雀 ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="乙女" src="<?php bloginfo('template_directory'); ?>/files/4e432e641050f84a2c4679440b72dbfb-300x199.png" alt="" width="300"
		height="199"></p>

<p style="text-align: center;">最後の青龍のいる八坂神社では子供にしか見られないと困っていた大人の乙女様に</p>

<p style="text-align: center;"><br style="text-align: center;"> 残り　５日　【前編】八坂神社
	/ 青龍?ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="かおるさん" src="<?php bloginfo('template_directory'); ?>/files/791489bace4a2c0908353e9a1870ba88-300x194.png" alt="" width="300"
		height="194"></p>

<p style="text-align: center;">J for Japsのユニフォームである「BLUE突撃隊」のパーカーを上げて、
	<del>卑猥なおじさん</del>
	大人の女性に見えるようにして、喜んでもらいました。
</p>
<p style="text-align: center;"><br style="text-align: center;"> 残り　４日　【後編】八坂神社
	/ 青龍?ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/96eb0442f60df2f177a01f608db2668b-300x198.png" alt="" width="300" height="198">
</p>

<p style="text-align: center;">染めヒト：ただここで、まさかの出来事が起こったんです。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="記者" src="<?php bloginfo('template_directory'); ?>/files/17811678c521b637083c254827155f2a-199x300.png" alt="" width="199"
		height="300"></p>

<p style="text-align: center;">記者：……………..。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>
<br>

<div style="text-align: center;"></div>
<div style="text-align: center;">
	<br>
	<img
		title="柴犬" src="<?php bloginfo('template_directory'); ?>/files/fbbad305fd0f232e8af5aac7b8350880-300x233.png" alt="" width="300"
		height="233"><br>
	<br>
	染めヒト：四神を倒せと言われたと思っていたら、なんと最後は柴犬だったんです。青龍を倒した後にこのことに気づきました。<br>
	<br>
	&nbsp;<br>
	<br>
	<img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"><br>
	<br>
	<img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/e06905e803226d808fbd00657938713b-300x200.png" alt="" width="300"
		height="200"><br>
	<br>
	あのときは本当に困ったんです。僕、犬大好きなんで、倒すなんて無理ってなって。<br>
	残り　３日　柴犬編<br>
	<br>
	<img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"><br>
	<br>
	<img
		title="ジェイさん、紳さん、ひろゆきさん" src="<?php bloginfo('template_directory'); ?>/files/ebc5b5f32aef04c1092b0a7e768a40a7-300x197.png" alt=""
		width="300" height="197"><br>
	<br>
	そしたらその姿をみた、LIGの岩上社長や、ジェイさん、紳さん、ひろゆきさんが助けてくれて、<br>
	<br>
	<img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"><br>
	<br>
	<img
		title="黒柴犬" src="<?php bloginfo('template_directory'); ?>/files/25cdab8366bb01b6265c5f0a52520a1c-226x300.png" alt="" width="226"
		height="300"><br>
	<br>
	僕のいないところで柴犬は倒されていたんです。<br>
	<br>
	<img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"><br>
	<br>
	&nbsp;<br>
	<br>
	<img
		class="aligncenter size-medium wp-image-868" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/30dccc429181eab62d3f097bc317b837-199x300.png" alt="" width="199" height="300"><br>
	<br>
</div>
<p style="text-align: center;">この時点でもう僕が出来る事はなくなりました。</p>

<p style="text-align: center;">このJ for Japsのプロジェクトは
	<del>友人</del>
	困った人の手助けや、LIGの皆様（勝手に）の協力もあってやっと、四神の４体をいなくなくならせて、柴犬も倒す事ができたんです。これが１ヶ月前に起こったJ for Japsのお話ですね。
</p>
<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-869" title="記者"
		src="<?php bloginfo('template_directory'); ?>/files/17811678c521b637083c254827155f2a-199x300.png" alt="" width="199" height="300"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-864" title="インタビュー"
		src="<?php bloginfo('template_directory'); ?>/files/edad1accfaf8ef1027f7a3c39910a495-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">記者：なるほど。説明有難う。最後に一つ聞きたいんだけど、その後そもそもの目的であったジェイさんの衣類は染める事が出来たのかな？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-871" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/5d7c2c0ffd7d53ea7f5bde870f3df1d4-300x201.png" alt="" width="300" height="201"></p>

<p style="text-align: center;">染めヒト：そのことについては１１月１５日に公開した（する）「染めヒトからジェイさんへの手紙」にジェイさんがコメントしてくれましたよ。それを読んで下さい。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-877" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/970bd0212fc269e0175141acc1050bae-204x300.png" alt="" width="204" height="300"></p>

<p style="text-align: center;">記者：わかった。楽しみにして明日の夜にでも見るよ、今日は染めヒトくん有難う。</p>

<p style="text-align: center;">染めヒト：いえいえ、こちらこそ有難うございました。ところでなんの媒体に載るんですか？ゲイナーですか？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-876" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/9e27aaca140f352004be8bebf87ec61b-199x300.png" alt="" width="199" height="300"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><strong>一ヶ月後のインタビュー終了</strong></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><strong>明日J for Japs最後の記事</strong></p>

<p style="text-align: center;"><strong>染めヒトからジェイさんへの手紙へ<br>
		2012.12.28日更新<br>
		<br>
	</strong></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">———————————-</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><strong><img
			class="aligncenter size-medium wp-image-861" title="記者"
			src="<?php bloginfo('template_directory'); ?>/files/f6b30ef9b48ec403c9cd31fbd4b34374-200x300.png" alt="" width="200" height="300">
		この度は本当に名前すら紹介してもらえていない、名前もしらない素性もしらない記者の方、誠に有難うございました。唯一知っている事は、撮影した日は週力活動中だったということだけです。記者の方に負けないように、僕も頑張ります。有難うございました。</strong>
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">※株式会社LIGの皆様。勝手に掲載しております。もし削除依頼があれば、すぐに削除さ
	せていただきますので、ご一報をお願い致します。勝手に、大変申し訳有りません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/"><img title="Ai for Japs"
																			src="<?php bloginfo('template_directory'); ?>/files/6b7307149d317a5ee0aa3ef496d160e1-300x150.png"
																			alt="" width="300" height="150"></a>
</p>

<p style="text-align: center;">京都の小さな手染め工房 Japan ○ Colorsでは、皆様の衣類に藍を吹き込む<a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/">Ai for Japs</a>というサービスをリリースいたしました。</p>

<p></p>
</div>
</div>
<!-- END .post -->


<div class="post">
<h2 class="title no_date">残り３日！柴犬編</h2>

<div class="post_content cf">
<p>
	&nbsp;<br>
</p>

<p style="text-align: center;"><a

		href="http://liginc.co.jp/"><img title="株式会社LIG"
										 src="<?php bloginfo('template_directory'); ?>/files/82f1b671c9423479b38dc54d8b46fe3b-300x136.png" alt=""
										 width="300" height="136"></a></p>

<p style="text-align: center;">株式会社LIGの広報担当ジェイさんの服を染めたい。<br>
	そのためには四神を倒さないといけない。<br>
	そんなところから始まったJ for Japs。<br>
	１３日目の昼が迎えようとしていた。</p>

<p style="text-align: center;"><a
		title="株式会社 LIG"
		href="http://liginc.co.jp/" target="_blank">http://liginc.co.jp/</a></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;">※話の道筋はこちらから<br>
	残り１４日　J for Japs
	の全てはここから始まった<br>
	残り１３日　モダンタイムズで緊急会議<br>
	残り１２日　これからの動きを決めるのだ<br>
	残り１１日　【前編】上賀茂神社 / 玄武ver<br>
	残り１０日　【後編】上賀茂神社 /
	玄武ver<br>
	残り　９日　【前編】松尾大社 / 白虎ver<br>
	残り　８日　【後編】松尾大社 / 白虎ver<br>
	残り　７日　【前編】城南宮 / 朱雀ver<br>
	残り　６日　【後編】城南宮 / 朱雀 ver<br>
	残り　５日　【前編】八坂神社 / 青龍?ver<br>
	残り　４日　【後編】八坂神社 / 青龍?ver</p>

<p style="text-align: center;">————————————————-</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-835" title="八坂神社"
		src="<?php bloginfo('template_directory'); ?>/files/8c4fe7b24a67fbbc16cddf56bd90d7c8-300x196.png" alt="" width="300" height="196"></p>

<p style="text-align: center;">京都：ざわざわっ、ざわざわっ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-826" title="狛犬"
		src="<?php bloginfo('template_directory'); ?>/files/63e11f9692113c2041fd88bf31f3f5e6-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">京都：ざわざわっ、ざわざわっ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-818" title="柴犬"
		src="<?php bloginfo('template_directory'); ?>/files/fbbad305fd0f232e8af5aac7b83508801-300x233.png" alt="" width="300" height="233"></p>

<p style="text-align: center;">京都：ざわざわっ、ざわざわっ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-811" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/33b20206c4194d5f0e64a3e79fd90fd9-300x199.png" alt="" width="300" height="199">染めヒト：…………….。
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/33b20206c4194d5f0e64a3e79fd90fd9-300x199.png" alt=""
		width="300" height="199"></p>

<p style="text-align: center;">染めヒト：…………………………。<br>
	かおるさん：染めヒトさんフリーズしすぎ！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-819" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/c8f9f40f936de727b2722c7a66588b9c-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">染めヒト：本気で困った。。。柴犬を倒すって、今更無理だって！！<br>
	かおるさん：染めヒトさん諦めないで下さい！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-828" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/e06905e803226d808fbd00657938713b-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">染めヒト：無理だってー！だってもう写真のストックがないから記事かけないもん！<br>
	かおるさん：写真のストック？何の話ですか！頑張りましょうよ！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-824" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/df977d376d512b89ef45033990c4af70-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">染めヒト：無理だって！柴犬って京都に全然いないってジェイさんもいってたもん！！<br>
	かおるさん：いやいや染めヒトさんの愛犬も柴犬だったし、近所に４匹くらいいましたよね。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-832" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/e2ccfff3d9a73986750e49dc339e62ac-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：嗚呼！！犬大好きなんだよ！倒せなんて無理だろー！<br>
	かおるさん：最後の最後なんです！ココは頑張りましょうよ！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/77ba6823067da16e73722ba20d4e87fa-300x196.png" alt="" width="300" height="196">
</p>

<p style="text-align: center;">染めヒト：はっ！！<br>
	かおるさん：どうしました！！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/d937d3fdd2d6c5053c2e852c6499b8ed-300x197.png" alt="" width="300" height="197">
</p>

<p style="text-align: center;">染めヒト：もういっそ柴犬気づかなかったってことにしない？<br>
	かおるさん：…..いやもう書いてますし、それに、</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-833" title="柴犬"
		src="<?php bloginfo('template_directory'); ?>/files/77b48c7ac23546a4be14c603e1582750-230x300.png" alt="" width="230" height="300"></p>

<p style="text-align: center;">かおるさん：Mr.片切さんが柴犬のくだりをきちんと覚えていますよ！Mr.片切さんの目はごまかせませんって！<br>
	染めヒト：かたぎりさーん！！！</p>

<p style="text-align: center;"><strong>※片切さん、勝手に名前使ってすみません。。</strong></p>
&nbsp;<br>
<br>
<img
	src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
	alt="" width="521" height="154"><br>

<p style="text-align: center;"><strong><br>
	</strong></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/e06905e803226d808fbd00657938713b-300x200.png" alt="" width="300" height="200">
</p>

<p style="text-align: center;">染めヒト：もうだめだー！！もうジャストアイデアがほしい！！！<br>
	かおるさん：…岩上社長じゃないんですから無理ですよ！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-836" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/77ba6823067da16e73722ba20d4e87fa-300x196.png" alt="" width="300" height="196"></p>

<p style="text-align: center;">染めヒト：はっ！！<br>
	かおるさん：またまたどうしたんですか！！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-812" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/d937d3fdd2d6c5053c2e852c6499b8ed-300x197.png" alt="" width="300"
		height="197">染めヒト：うん！前々回の乙女さんの記事で、子犬のくだりなかった！？<br>
	かおるさん：….ありましたけど?</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-820" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/67d0755de4a1b6993f540ef4ff2458eb-300x195.png" alt="" width="300" height="195"><br>
	染めヒト：もういっそ乙女さん柴犬っていう設定に書き換えない？<br>
	かおるさん：いや、もう乙女って記事書いてますし….</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-828" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/e06905e803226d808fbd00657938713b-300x200.png" alt="" width="300" height="200">染めヒト：かおるさんのいじわるー！もう無理だ！だーー！<br>
	かおるさん：染めヒトさん頑張って！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/77ba6823067da16e73722ba20d4e87fa-300x196.png" alt="" width="300" height="196">
</p>

<p style="text-align: center;">染めヒト：はっ！<br>
	かおるさん：ジャストアイデアですか！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/d937d3fdd2d6c5053c2e852c6499b8ed-300x197.png" alt="" width="300" height="197">
</p>

<p style="text-align: center;">染めヒト：ううん、これ最初から考えてた案なんだけど、</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="狛犬" src="<?php bloginfo('template_directory'); ?>/files/63e11f9692113c2041fd88bf31f3f5e6-300x197.png" alt="" width="300"
		height="197"></p>

<p style="text-align: center;">染めヒト：京都と言えば神社、神社と言えば狛犬だよね！？<br>
	かおるさん：そうですけど….</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-515" title="桜子さん"
		src="<?php bloginfo('template_directory'); ?>/files/e72aeed55676de7cb743932917983fe0-300x196.png" alt="" width="300" height="196"></p>

<p style="text-align: center;">染めヒト：桜子さんの服、紫に染めたし、あの服を狛犬に着せて柴犬ってことじゃだめかな！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-815" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/a65c9cad18d65bf0db8e449b7e0c7b14-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">かおるさん：<br>
	染めヒトさん……最初からきちんとわかっていたんですね。……….でもそれって漢字違いますよ？柴犬の柴は下が「木」で、紫は「糸」ですから「しばいぬ」じゃなくて「むらさきいぬ」になりますよ？</p>

<p style="text-align: center;">染めヒト：だいごさーーーーーん！！！！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/e2ccfff3d9a73986750e49dc339e62ac-300x199.png" alt="" width="300" height="199">
</p>

<p style="text-align: center;">染めヒト：無理だー！国語嫌いがここにきてあだとなったーー！！せめて数学にしてほしかった！！<br>
	かおるさん：数学！！？なんの話ですか！！？頑張りましょうよ！！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-814" title="ゴウさんが売っているもの"
		src="<?php bloginfo('template_directory'); ?>/files/9e16f09ef32537560b8894a05985c775-300x198.png" alt="" width="300" height="198"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="狛犬" src="<?php bloginfo('template_directory'); ?>/files/63e11f9692113c2041fd88bf31f3f5e6-300x197.png" alt="" width="300"
		height="197"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"></p>
<img class="aligncenter size-medium wp-image-841" title="空"
	 src="<?php bloginfo('template_directory'); ?>/files/6943e4f690e9caac7ef08f3a869427ba-300x215.png" alt="" width="300" height="215">

<p style="text-align: center;">その頃……東京の台東区では…..</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-821" title="岩上社長"
		src="<?php bloginfo('template_directory'); ?>/files/cbeb80ae93f9307d676d3c9a57c25aae-300x193.png" alt="" width="300" height="193"></p>

<p style="text-align: center;">LIG_社長：うん。染めヒトよく頑張ったと思う。ジェイ、なんとか柴犬を倒して上げてくれないか？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-830" title="ジェイさん"
		src="<?php bloginfo('template_directory'); ?>/files/e472654f5e0e649445469e9d61ef4bc8-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">ジェイさん：………無理。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-827" title="社長"
		src="<?php bloginfo('template_directory'); ?>/files/3bdf1ede0e3c7378d00f61d73e618cd2-300x198.png" alt="" width="300" height="198"><br>
	LIG_社長：ふんぬーーーーーーーー！！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-838" title="ジェイさん"
		src="<?php bloginfo('template_directory'); ?>/files/97391819a8ad8d9970cab50f2407152e-300x199.png" alt="" width="300" height="199">ジェイさん：…….わかった。
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-816" title="ジェイさん"
		src="<?php bloginfo('template_directory'); ?>/files/6eeea8018846309593e2c208e8626d85-300x198.png" alt="" width="300" height="198">ジェイさん：Mr.青龍、お願い。
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-831" title="青龍"
		src="<?php bloginfo('template_directory'); ?>/files/1fe39318c41dad085466c4d2a528b0b7-300x175.png" alt="" width="300" height="175"></p>

<p style="text-align: center;">青龍：…….無理。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-817" title="ジェイさん、紳さん、ひろゆきさん"
		src="<?php bloginfo('template_directory'); ?>/files/ebc5b5f32aef04c1092b0a7e768a40a7-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">ジェイさん、紳さん、ひろゆきさん：…..頼む。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="青龍" src="<?php bloginfo('template_directory'); ?>/files/1fe39318c41dad085466c4d2a528b0b7-300x175.png" alt="" width="300"
		height="175"></p>

<p style="text-align: center;">青龍：…….わかった。ちょっとまって。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-841" title="空"
		src="<?php bloginfo('template_directory'); ?>/files/6943e4f690e9caac7ef08f3a869427ba-300x215.png" alt="" width="300" height="215"></p>
&nbsp;<br>
<br>
<img
	src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
	alt="" width="521" height="154"><br>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-839" title="黒柴犬"
		src="<?php bloginfo('template_directory'); ?>/files/0be31a57b3b90f20a470e1c856c72906-221x300.png" alt="" width="221" height="300"></p>

<p style="text-align: center;">黒柴：…….ワン！<br>
	飼い主：どうしたの？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="青龍" src="<?php bloginfo('template_directory'); ?>/files/1fe39318c41dad085466c4d2a528b0b7-300x175.png" alt="" width="300"
		height="175"></p>

<p style="text-align: center;">青龍：んんんんんん、ふん！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-823" title="黒柴犬"
		src="<?php bloginfo('template_directory'); ?>/files/25cdab8366bb01b6265c5f0a52520a1c-226x300.png" alt="" width="226" height="300"></p>

<p style="text-align: center;">黒柴犬：くーーーん。。<br>
	飼い主：え！！なんで急に衣類が染まったの！！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="青龍" src="<?php bloginfo('template_directory'); ?>/files/1fe39318c41dad085466c4d2a528b0b7-300x175.png" alt="" width="300"
		height="175"></p>

<p style="text-align: center;">青龍：ジェイ、一応黒柴犬だけど、倒しといたよ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-842" title="ジェイさん"
		src="<?php bloginfo('template_directory'); ?>/files/3211ef5a3956ba3db66ef10e27c4fe5c-300x176.png" alt="" width="300" height="176"></p>

<p style="text-align: center;">ジェイさん：Thanks Mr.青龍！タカ、これでいいかい？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-843" title="社長"
		src="<?php bloginfo('template_directory'); ?>/files/9124cf59caddfbe78b7d11781f7829d5-300x193.png" alt="" width="300" height="193"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="空" src="<?php bloginfo('template_directory'); ?>/files/6943e4f690e9caac7ef08f3a869427ba-300x215.png" alt="" width="300"
		height="215"></p>

<p style="text-align: center;"><span style="font-size: 14px;"><strong>その頃京都では…….</strong></span></p>
<img
	src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
	alt="" width="521" height="154"><br>

<p style="text-align: center;"><img
		title="狛犬" src="<?php bloginfo('template_directory'); ?>/files/63e11f9692113c2041fd88bf31f3f5e6-300x197.png" alt="" width="300"
		height="197"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/e06905e803226d808fbd00657938713b-300x200.png" alt="" width="300" height="200">
</p>

<p style="text-align: center;"><span style="font-size: 14px;"><strong>まだ染めヒトが困っていた。<br>
			飼い主から連絡があり、倒せたことを知るのは、<br>
			もう少し後のことだった。</strong></span></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><span style="font-size: 16px;"><strong>柴犬編、終了</strong></span></p>

<p style="text-align: center;"><span style="font-size: 16px;"><strong><img
				src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
				alt="" width="521" height="154"><br>
		</strong></span></p>
&nbsp;<br>

<p style="text-align: center;"><span style="font-size: 16px;"><strong>残り２日</strong></span></p>
&nbsp;<br>

<p style="text-align: center;"><span style="font-size: 16px;"><strong>最終編「LIGへの手紙ver」</strong></span></p>

<p style="text-align: center;"></p>
&nbsp;<br>

<p style="text-align: center;">———————————–</p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-844" title="片切さん"
		src="<?php bloginfo('template_directory'); ?>/files/e2c7ebd9ccad0519809be02308435d93-300x209.png" alt="" width="300" height="209"></p>

<p style="text-align: center;"><strong>今回勝手に出させていただいたTwitterで柴犬のことを書いて下さった見ず知らずの片切様、誠に有難うございました。片切様の目が気になって、柴犬編なんとか書き上げました。有難うございました。</strong>
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>
&nbsp;<br>

<p style="text-align: center;">
	※株式会社LIGの皆様。勝手に掲載しております。もし削除依頼があれば、すぐに削除させていただきますので、ご一報をお願い致します。勝手に、大変申し訳有りません。また今回岩上社長ならびに、Jさん、紳さん、ひろゆきさんと写真を勝手に使わせていただきました。誠に申し訳ございませんでした。本当にすみません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;"><a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/"><img title="Ai for Japs"
																			src="<?php bloginfo('template_directory'); ?>/files/6b7307149d317a5ee0aa3ef496d160e1-300x150.png"
																			alt="" width="300" height="150"></a>
</p>

<p style="text-align: center;">京都の小さな手染め工房 Japan ○ Colorsでは、皆様の衣類に藍を吹き込む<a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/">Ai for Japs</a>というサービスをリリースいたしました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/0a779bed1bfad809fc429c2eff8af708.png"
		alt="" width="521" height="154"></p>

<p style="text-align: center;">
	八坂神社は京都の中心にそびえ立つ大きな神社です。平日でも屋台が出ており、近くには清水寺、南禅寺、また京都の繁華街である四条河原町や京の台所錦市場もございます。また陽気なおっちゃんもおられます。京都に来た際には是非八坂神社へおこしやす。<br>
	→<a
		title="八坂神社" href="http://web.kyoto-inet.or.jp/org/yasaka/">http://web.kyoto-inet.or.jp/org/yasaka/</a>
</p>

<p></p>
</div>
</div>
<!-- END .post -->


<div class="post">
<h2 class="title no_date">残り４日　【後編】八坂神社 / 青龍?ver</h2>

<div class="post_content cf">
<p>
</p>

<p style="text-align: center;"><a

		href="http://liginc.co.jp/"><img title="株式会社LIG"
										 src="<?php bloginfo('template_directory'); ?>/files/82f1b671c9423479b38dc54d8b46fe3b-300x136.png" alt=""
										 width="300" height="136"></a></p>

<p style="text-align: center;">株式会社LIGの広報担当ジェイさんの服を染めたい。<br>
	そのためには四神を倒さないといけない。<br>
	そんなところから始まったJ for Japs。<br>
	11日目の昼が迎えようとしていた。</p>

<p style="text-align: center;"><a
		title="株式会社 LIG"
		href="http://liginc.co.jp/" target="_blank">http://liginc.co.jp/</a></p>

<p style="text-align: center;"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">※話の道筋はこちらから<br>
	残り１４日　J for Japs
	の全てはここから始まった<br>
	残り１３日　モダンタイムズで緊急会議<br>
	残り１２日　これからの動きを決めるのだ<br>
	残り１１日　【前編】上賀茂神社 / 玄武ver<br>
	残り１０日　【後編】上賀茂神社 /
	玄武ver<br>
	残り　９日　【前編】松尾大社 / 白虎ver<br>
	残り　８日　【後編】松尾大社 / 白虎ver<br>
	残り　７日　【前編】城南宮 / 朱雀ver<br>
	残り　６日　【後編】城南宮 / 朱雀 ver<br>
	残り　５日　【前編】八坂神社 / 青龍?ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;">————————————————-</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-790" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/72800567ae3a861a135dd7276072d91a1-300x200.png" alt="" width="300" height="200">かおるさん：染めヒトさん、子供に見られて困っていたあの乙女さん、どうするんですか？
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-771" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/3ecec131006d44961e5f05de86c97614-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">
	染めヒト：うん、ちょっと考えたんだけど、今まで玄武、白虎、朱雀をいなくならせてきたんだけどね、結局３人しか困った人を救えてないんだ。それが何か物足りないって、ジェイさん思ってると思うんだ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-794" title="BLUE突撃隊"
		src="<?php bloginfo('template_directory'); ?>/files/110051cb2e670082891f48b7bc1231ac1-300x199.png" alt="" width="300" height="199">染めヒト：だから、このBLUE突撃隊のユニフォームを継承しようと思うんだ。これを着れば、この先乙女さんは京都の困った人を助ける宿命を背負うことになる。しかも手染めで。良い、営業マンになるんだ。そして徐々に京都はもっとハッピーになるんだ！J
	for Japsが芽となって、幸せが広がるんだ！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"><br>
	<img
		class="aligncenter size-medium wp-image-786" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/3ecec131006d44961e5f05de86c976141-300x199.png" alt="" width="300" height="199">染めヒト：<br>
	しかもこのユニフォームの凄いところは、どんな人が着ても、僕みたいな卑しいおっさんに見えるんだ。さっきも大学生っぽい人に笑われたしね。そして凄いことに、乙女が着ても小さなおっさんに見えるんだ！彼女の子供に見られる悩みは解消出来るはずだよ。
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-790" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/72800567ae3a861a135dd7276072d91a1-300x200.png" alt="" width="300" height="200">かおるさん：…………（鬼だなこの人。）
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-776" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/520aad338332280776637326bca41321-300x195.png" alt="" width="300" height="195"><br>
	乙女：………..。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-780" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/4d6be53c0905bab260d87e3c33eab0f7-300x196.png" alt="" width="300" height="196"></p>

<p style="text-align: center;">染めヒト：おーい、乙女さん！お待たせ！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-779" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/8bdbd4ca2e50cc767eebe0144c407c57-300x195.png" alt="" width="300" height="195"></p>

<p style="text-align: center;">乙女：ああ、どうもこんにちは。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-795" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/095f1f5b4280102947862bc313302f59-300x197.png" alt="" width="300" height="197"><br>
	染めヒト：どうも。さっそくですが、ちょっといいでしょうか。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-791" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/ee3158929c47e2713d5f3b046ed5ab6e-300x200.png" alt="" width="300" height="200">染めヒト：こちらの服を着て、おっさんに見られて、なおかつ京都にいる困った人を助けて下さい！<br>
	かおるさん：…….（無茶苦茶だな。）</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-799" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/48b3173949b4c20558c8a24f0043accc-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">乙女：はい？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-791" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/ee3158929c47e2713d5f3b046ed5ab6e-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">染めヒト：いいから着て下さい。子供っぽさからおっさんへ、そして京都の困ったちゃんを救って上げて下さい。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-784" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/1d99854fb7ec486a417bda1d581ead7b-300x198.png" alt="" width="300" height="198"></p>

<p style="text-align: center;">乙女：絶対にいや！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-791" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/ee3158929c47e2713d5f3b046ed5ab6e-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">染めヒト：…………………。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-781" title="乱闘"
		src="<?php bloginfo('template_directory'); ?>/files/c056d78cb69f3264d911c98e8765b101-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">染めヒト：いいから着ろっつってんだよ！<br>
	乙女：いやーーーー！！<br>
	かおるさん：誰にも見られてません！今です染めヒトさん！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">……..１分後…..</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-777" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/e0fec7d27d2b465dcd8c190420a9f0d7-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">染めヒト：うん、いい感じにでかくて、フードかぶると乙女には見えない。<br>
	かおるさん：…………。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-775" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/79335d880f466302cc2480a07c54ef2d-300x198.png" alt="" width="300" height="198">染めヒト：うん、後ろを向くと完全に小さなおっさんだ！もうこれで、完全に子供から脱却だね！<br>
	かおるさん：…………（可哀想。）</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-795" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/095f1f5b4280102947862bc313302f59-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">染めヒト：どうですか乙女さん、これであなたの悩みは解消されましたし、京都の困った人を助けるという宿命を背負った。一石二鳥です。乙女さん、最高ですね！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-777" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/e0fec7d27d2b465dcd8c190420a9f0d7-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">乙女：…………。すぐ脱ぐし、意味のわからない宿命も背負いませんよ？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-788" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/0a1e2c4fb60aeec18b53242b64a409f4-300x200.png" alt="" width="300" height="200"></p>

<p style="text-align: center;">染めヒト：仕方ないな。。。かおるさん、君の出番だ！かおるスペシャルを見せてやれ！<br>
	かおるさん：え、あ、はい。えぇ….気が進みませんが………</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-792" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/791489bace4a2c0908353e9a1870ba88-300x194.png" alt="" width="300" height="194"></p>

<p style="text-align: center;">かおるさん：<br>
	さっきから黙って聞いていりゃよ、お前、染めヒトさんが染めた衣類を着れないっていうのか？あぁ”？すぐ脱ぐ？あぁ”？ちゃんとその服着て大人に見られて、京都をもっと良くしろっつってんだよ。それで青龍いなくなるんだよ。とりあえずこのブログ書き終わるまでは、着続けろよジェイさん見てるんだから。
</p>

<p style="text-align: center;">（内心：ごめんなさい！ごめんなさい！本当にごめんなさい！！）</p>

<p style="text-align: center;">乙女：…………（近寄られたら本気で怖い..）。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-785" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/47b0a6a27dc4289f4ef2fc75d9436798-300x195.png" alt="" width="300" height="195"></p>

<p style="text-align: center;">乙女：<br>
	すみませんでした。本当にすみませんでした。大人に見られて嬉しいです。京都の困った人救います。有難うございました。有難うございました。すみませんでした。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-782" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/c17b7be3df15ace611f4ce97230f5b8f-300x194.png" alt="" width="300" height="194"></p>

<p style="text-align: center;">染めヒト：うん、それでいいんだ！染めヒトとして、ハッピーだよ！<br>
	かおるさん：……..。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-778" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/6fd5c666fae0fca96c130d398faf6604-300x199.png" alt="" width="300" height="199">染めヒト：よしよし。これでジェイさんの指示通り四天王である四神は倒したと！あとはTwitterで報告するだけだ！<br>
	かおるさん：………（あんなことして、ばちあたるんじゃないのかな）</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-774" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/9f692847e829d67fedbd87d69612bcb2-300x198.png" alt="" width="300" height="198"></p>

<p style="text-align: center;">染めヒト：ん！？<br>
	かおるさん：どうしたんですか？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-789" title="ジェイさん"
		src="<?php bloginfo('template_directory'); ?>/files/b283d57da4a99b8da1e3bf29117b1364-228x300.png" alt="" width="228" height="300"></p>

<p style="text-align: center;">染めヒト：うん、ジェイさんが最初に指示してくれたTweetを改めて今見返したんだ。<br>
	かおるさん：それがどうかしたんですか？<br>
	染めヒト：うん。今まで玄武、白虎、朱雀、そして今回の青龍を倒しせばジェイさんの衣類を染められると。。<br>
	かおるさん：え？それがどうかしたんですか？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-789" title="ジェイさん"
		src="<?php bloginfo('template_directory'); ?>/files/b283d57da4a99b8da1e3bf29117b1364-228x300.png" alt="" width="228" height="300"></p>

<p style="text-align: center;">染めヒト：うん、かおるさん、冷静によく見て。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-803" title="柴犬"
		src="<?php bloginfo('template_directory'); ?>/files/fbbad305fd0f232e8af5aac7b8350880-300x233.png" alt="" width="300" height="233"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;">…………………。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;">………………………。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>
<br>
<br>

<div id="attachment_793" class="wp-caption aligncenter" style="width: 658px"><img class="size-full wp-image-793"
																				  title="紳さん"
																				  src="<?php bloginfo('template_directory'); ?>/files/5a36416946b106295a95d17a8762ccf6.png"
																				  alt="" width="648" height="427">

	<p class="wp-caption-text"><a

			title="【LIGのキノコ狩り】秋の社員旅行のお知らせ（今月11、12日は臨時休業となります）" href="http://liginc.co.jp/news/archives/620">【LIGのキノコ狩り】秋の社員旅行のお知らせより。</a>
	</p></div>
<br>

<p style="text-align: center;">染めヒト：違った。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img class="aligncenter size-full wp-image-797"
									title="染めヒト"
									src="<?php bloginfo('template_directory'); ?>/files/fdae39ff0d7437e4d9b91e43d5ed2327.png"
									alt="" width="530" height="354"></p>

<p style="text-align: center;">染めヒト：（紳さんのあの顔、難しい。。）<br>
	かおるさん：（染めヒトさん、紳さんのくだりは、キノコ狩りの記事みないとわからないのに。。）</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-804" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/df58fbc1846331306fa42251ee0de3cf-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">染めヒト：嗚呼、もうだめだ。残り三日で柴犬を倒すなんて、犬好きな僕には出来ない。。<br>
	かおるさん：染めヒトさん……….</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">Pinchi…..</p>

<p style="text-align: center;">to be continue…….</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">———————————–</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><strong><span style="font-size: 18px;">残り、３日……。</span></strong></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-777" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/e0fec7d27d2b465dcd8c190420a9f0d7-300x197.png" alt="" width="300" height="197"></p>

<p style="text-align: center;">
	今回わざわざ八坂神社に着ていただいた少女に見える大人の乙女様、誠に有難うございました。BLUE突撃隊のユニフォームは誰でもおっさんに見えることを証明していただき、誠に有難うございました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">
	※株式会社LIGの皆様。勝手に掲載しております。もし削除依頼があれば、すぐに削除させていただきますので、ご一報をお願い致します。勝手に、大変申し訳有りません。Jさん、紳さん、本当にすみません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/"><img title="Ai for Japs"
																			src="<?php bloginfo('template_directory'); ?>/files/6b7307149d317a5ee0aa3ef496d160e1-300x150.png"
																			alt="" width="300" height="150"></a>
</p>

<p style="text-align: center;">京都の小さな手染め工房 Japan ○ Colorsでは、皆様の衣類に藍を吹き込む<a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/">Ai for Japs</a>というサービスをリリースいたしました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">
	八坂神社は京都の中心にそびえ立つ大きな神社です。平日でも屋台が出ており、近くには清水寺、南禅寺、また京都の繁華街である四条河原町や京の台所錦市場もございます。また陽気なおっちゃんもおられます。京都に来た際には是非八坂神社へおこしやす。<br>
	→<a
		title="八坂神社" href="http://web.kyoto-inet.or.jp/org/yasaka/">http://web.kyoto-inet.or.jp/org/yasaka/</a>
</p>

<p></p>
</div>
</div>
<!-- END .post -->


<div class="post">
<h2 class="title no_date">残り５日　【前編】八坂神社 / 青龍？ver</h2>

<div class="post_content cf">
<p>
</p>

<p style="text-align: left;"><a

		href="http://liginc.co.jp/"><img class="aligncenter" title="株式会社LIG"
										 src="<?php bloginfo('template_directory'); ?>/files/82f1b671c9423479b38dc54d8b46fe3b-300x136.png" alt=""
										 width="300" height="136"></a></p>

<p style="text-align: center;">株式会社LIGの広報担当ジェイさんの服を染めたい。<br>
	そのためには四神を倒さないといけない。<br>
	そんなところから始まったJ for Japs。<br>
	10日目の昼が迎えようとしていた。</p>

<p style="text-align: center;"><a
		title="株式会社 LIG"
		href="http://liginc.co.jp/" target="_blank">http://liginc.co.jp/</a></p>

<p style="text-align: center;"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">※話の道筋はこちらから<br>
	残り１４日　J for Japs
	の全てはここから始まった<br>
	残り１３日　モダンタイムズで緊急会議<br>
	残り１２日　これからの動きを決めるのだ<br>
	残り１１日　【前編】上賀茂神社 / 玄武ver<br>
	残り１０日　【後編】上賀茂神社 /
	玄武ver<br>
	残り　９日　【前編】松尾大社 / 白虎ver<br>
	残り　８日　【後編】松尾大社 / 白虎ver<br>
	残り　７日　【前編】城南宮 / 朱雀ver<br>
	残り　６日　【後編】城南宮 / 朱雀 ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png" alt="" width="658" height="169"></p>

<p style="text-align: center;">————————————————-</p>

<p style="text-align: center;"><img
		class="size-medium wp-image-751 aligncenter" title="八坂神社"
		src="<?php bloginfo('template_directory'); ?>/files/de1435ff3c89fea74f7570f0323b1839-300x195.png" alt="" width="300" height="195"></p>

<p style="text-align: center;">京都：ざわざわっ。ざわざわっ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-753" title="狛犬"
		src="<?php bloginfo('template_directory'); ?>/files/da7eb7437aff3260d4159fa1d20a1f29-300x194.png" alt="" width="300" height="194"><br>
	京都：ざわざわっ。ざわざわっ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-754" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/24f603ded8f1e3818d917e04233ec0da-300x196.png" alt="" width="300" height="196"><br>
	染めヒト：うん、昼間はだめだ。人が多すぎてマネキンなんてもてない。<br>
	かおるさん：……すみません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-755" title="八坂神社"
		src="<?php bloginfo('template_directory'); ?>/files/da008b94bb9db310e6f6c7e8a4f08269-300x197.png" alt="" width="300" height="197"><br>
	染めヒト：それにしてもいつでも盛り上がってるなー。<br>
	かおるさん：出店が結構ありますね！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-757" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/0067679af7f23a4c116f54de1d17ba67-300x197.png" alt="" width="300" height="197"><br>
	かおるさん：出店をみると、お祭りにきたみたいで少しわくわくしますね！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-756" title="出店の人"
		src="<?php bloginfo('template_directory'); ?>/files/3e342cb37577edca7703ac5f64bc4499-300x198.png" alt="" width="300" height="198">おっちゃん：兄ちゃん、面白いお面もってるじゃんか！<br>
	染めヒト：それかぶれませんよ？<br>
	かおるさん：渡さないで下さい！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-743" title="うさぎ"
		src="<?php bloginfo('template_directory'); ?>/files/adb28d6cb94a7966d7c0908f9549d4bb-300x199.png" alt="" width="300" height="199"><br>
	かおるさん：うわー有名な八坂神社の「大国様と白兎」ですね！可愛い！<br>
	染めヒト：………良い事思いついた。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-741" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/c60aaebff085116dc14ac8ec6020cb3e-300x199.png" alt="" width="300" height="199"><br>
	染めヒト：新しい「大国様とマネキン」などいかがでしょう？<br>
	かおるさん：ちょっとやめてください！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-742" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/028d01e0e7f4ca0d9eac4a36c8c6c55a-300x198.png" alt="" width="300" height="198"><br>
	かおるさん：ちょ、ちょっと染めヒトさん！人が集まってきましたよ！！<br>
	染めヒト：さあ、次にいこうか。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-745" title="絵馬"
		src="<?php bloginfo('template_directory'); ?>/files/dcc126eae27b807b277c5fbf24472b64-300x196.png" alt="" width="300" height="196"><br>
	染めヒト：絵馬でもかいて、願い叶えてもらうか。<br>
	かおるさん：そうですね！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-746" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/dc35ff1f81891c2abd4b78b1e0b64d08-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：蒼井優ちゃんと僕の熱愛がフライデーにのりませんようにっと。<br>
	かおるさん：え、あ、はい。ないと思いますよ？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-747" title="スクリーンショット 2012-11-11 8.38.07"
		src="<?php bloginfo('template_directory'); ?>/files/fb823d1e49c741419a172cc9b783c8dc-300x198.png" alt="" width="300" height="198"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-732" title="染めヒトかくれる"
		src="<?php bloginfo('template_directory'); ?>/files/1dbe26813db4f8524d58e423e18e4fe7-300x197.png" alt="" width="300" height="197">染めヒト：よし、じゃあ困っている人を探すか！あと一体?だからね！<br>
	かおるさん：はい、頑張りましょう！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-731" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/0bfb761ed155d5ca030dcec159d0dd99-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：おーい、困ってる人ー！！<br>
	かおるさん：人の目気にし過ぎす…..。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-732" title="染めヒトかくれる"
		src="<?php bloginfo('template_directory'); ?>/files/1dbe26813db4f8524d58e423e18e4fe7-300x197.png" alt="" width="300" height="197"><br>
	染めヒト；うん、人ごみさけたら全然人いなくて、見つかる気配がないね。<br>
	かおるさん：……..はい。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-735" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/fff9a37b81caa2e22ea4e5cc7767021e-300x197.png" alt="" width="300" height="197"><br>
	かおるさん：ん？なんかあそこにいますよ？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-738" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/f1c42ca04db8102a540e86ab7f3f15a7-300x197.png" alt="" width="300" height="197"><br>
	乙女：………。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-725" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/51c5001d34600fe9b061df8fdc714850-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：やばい！！！やばーーい！！！！<br>
	かおるさん：染めヒトさん！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-733" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/d40b33c697971d5c32117aef3743367f-300x198.png" alt="" width="300" height="198"></p>

<p style="text-align: center;">染めヒト：子犬だーー！！！<br>
	かおるさん：ひーー！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-726" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/957c5d45562fde90bdb7227ec3e94fd7-300x198.png" alt="" width="300" height="198"><br>
	かおるさん：あーーれーー。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-724" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/239da28325f1bf94b801df5bc8e27a7c-300x198.png" alt="" width="300" height="198"><br>
	乙女：キャーーー！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-736" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/7ffce507deed3cd1aa4bf5f76dc4700a-300x196.png" alt="" width="300" height="196"></p>

<p style="text-align: center;">染めヒト：この犬かわいーー！！よーしよし！！<br>
	乙女：イヤー！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-739" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/ca8e219dfb0406bf19fed7ffa179c444-300x198.png" alt="" width="300" height="198"><br>
	乙女：襲うのやめてください！何やってるんですか！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-749" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/e1045efcc633802eb7f22865d45c8c7c-300x199.png" alt="" width="300" height="199"><br>
	染めヒト：あれ？<br>
	かおるさん：染めヒトさん彼女は人間です！！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-721" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/f6d425480450320c1455a3ff3d18b615-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：これはどうも申し訳ございませんでした！<br>
	かおるさん：下手したら犯罪でしたよ！こんな小さい子供に！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-739" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/ca8e219dfb0406bf19fed7ffa179c444-300x198.png" alt="" width="300" height="198"></p>

<p style="text-align: center;">乙女：あのー、そこのマネキンさん。ワタシ、もう二十代中盤ですけど….。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-721" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/f6d425480450320c1455a3ff3d18b615-300x199.png" alt="" width="300" height="199"></p>

<p style="text-align: center;">染めヒト：………………。<br>
	かおるさん：……….あ、すみません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-727" title="乙女"
		src="<?php bloginfo('template_directory'); ?>/files/821bd7e3d77bf747de0f5829e128e640-300x198.png" alt="" width="300" height="198"><br>
	乙女<br>
	こちらこそ、すみません。いつも小学生に見られるんです。。まだ子供料金で電車に乗れます。ワタシはそれが嫌なんです。童顔が嫌なんです。子供に見られたくないんです。。凄く困ってます。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-737" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/0797719bad3e178026bfe21460ba7f3d-300x194.png" alt="" width="300" height="194">かおるさん：この人助けましょうよ！衣類でどうにかなりますよ！！<br>
	染めヒト：…..うん、ちょっとまって。。。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-737" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/0797719bad3e178026bfe21460ba7f3d-300x194.png" alt="" width="300" height="194">かおるさん：何を待つんですか！？<br>
	染めヒト：凄い良い案浮かんできそうなんだ。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/0797719bad3e178026bfe21460ba7f3d-300x194.png" alt=""
		width="300" height="194"></p>

<p style="text-align: center;">………………….</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/0797719bad3e178026bfe21460ba7f3d-300x194.png" alt="" width="300" height="194">
</p>

<p style="text-align: center;">染めヒト：全てがわかった。<br>
	かおるさん：え？まあ、いきましょう！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-734" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/2e7a0dadc4802a6173fb8c6eb98b405f-300x201.png" alt="" width="300" height="201">染めヒト：そこの乙女さん、その困りごと、ワタシにお任せ下さいませんか？<br>
	かおるさん：この方、手染めをする人なんです！衣類を染めて解決してくれますよ！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/0797719bad3e178026bfe21460ba7f3d-300x194.png" alt=""
		width="300" height="194"></p>

<p style="text-align: center;">染めヒト：いや、今回衣類はわざわざ染めなくて良いんだ。<br>
	かおるさん：えっ？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-720" title="染めヒト"
		src="<?php bloginfo('template_directory'); ?>/files/fab03e882f4029d6fd30ea3d78774bb0-300x199.png" alt="" width="300" height="199">染めヒト：そこの乙女さん、そこでしばらくお待ち下さいませんか？<br>
	かおるさん：染めヒトさん、何をする気ですか！？</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="乙女" src="<?php bloginfo('template_directory'); ?>/files/4e432e641050f84a2c4679440b72dbfb-300x199.png" alt="" width="300"
		height="199"></p>

<p style="text-align: center;">乙女：あ、はい。わかりました。ここでお待ちしています。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/fab03e882f4029d6fd30ea3d78774bb0-300x199.png" alt="" width="300" height="199">
</p>

<p style="text-align: center;">染めヒト：一応明日っていう設定なんで、それではまた明日！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-723" title="かおるさん"
		src="<?php bloginfo('template_directory'); ?>/files/5874a072410f3818983578fca915b84c-300x198.png" alt="" width="300" height="198">かおるさん：（染めヒトさん、何する気なんだろう….。）
</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">to be continue…….</p>

<p style="text-align: center;">———————————–</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">リアルタイムでStory作成、記事の配信中。<br>
	残りあとわずか。。。少しホっとしています。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>
<img
	class="aligncenter" title="乙女" src="<?php bloginfo('template_directory'); ?>/files/239da28325f1bf94b801df5bc8e27a7c-300x198.png" alt=""
	width="300" height="198"><br>

<p style="text-align: center;">
	今回わざわざ八坂神社に着ていただいた少女に見える大人の乙女様、誠に有難うございました。中2から服のサイズが変わらないと聞いたときは、ちょっと羨ましく思いました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">
	※株式会社LIGの皆様。勝手に掲載しております。もし削除依頼があれば、すぐに削除させていただきますので、ご一報をお願い致します。勝手に、大変申し訳有りません。Jさん、本当にすみません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/"><img title="Ai for Japs"
																			src="<?php bloginfo('template_directory'); ?>/files/6b7307149d317a5ee0aa3ef496d160e1-300x150.png"
																			alt="" width="300" height="150"></a>
</p>

<p style="text-align: center;">京都の小さな手染め工房 Japan ○ Colorsでは、皆様の衣類に藍を吹き込む<a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/">Ai for Japs</a>というサービスをリリースいたしました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">
	八坂神社は京都の中心にそびえ立つ大きな神社です。平日でも屋台が出ており、近くには清水寺、南禅寺、また京都の繁華街である四条河原町や京の台所錦市場もございます。また陽気なおっちゃんもおられます。京都に来た際には是非八坂神社へおこしやす。<br>
	→<a
		title="八坂神社" href="http://web.kyoto-inet.or.jp/org/yasaka/">http://web.kyoto-inet.or.jp/org/yasaka/</a>
</p>

<p></p>
</div>
</div>
<!-- END .post -->


<div class="post">
<h2 class="title no_date">残り６日！【後編】城南宮 / 朱雀ver</h2>

<div class="post_content cf">
<p>
</p>

<p style="text-align: center;"><a
		href="http://liginc.co.jp/"><img title="株式会社LIG"
										 src="<?php bloginfo('template_directory'); ?>/files/82f1b671c9423479b38dc54d8b46fe3b-300x136.png"
										 alt="" width="300" height="136"></a></p>

<p style="text-align: center;">株式会社LIGの広報担当ジェイさんの服を染めたい。<br>
	そのためには四神を倒さないといけない。<br>
	そんなところから始まったJ for Japs。<br>
	９日目の早朝が迎えようとしていた。</p>

<p style="text-align: center;"><a
		title="株式会社 LIG" href="http://liginc.co.jp/" target="_blank">http://liginc.co.jp/</a>
</p>

<p style="text-align: center;"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">※話の道筋はこちらから<br>
	残り１４日　J for Japs の全てはここから始まった<br>
	残り１３日　モダンタイムズで緊急会議<br>
	残り１２日　これからの動きを決めるのだ<br>
	残り１１日　【前編】上賀茂神社 / 玄武ver<br>
	残り１０日　【後編】上賀茂神社 / 玄武ver<br>
	残り　９日　【前編】松尾大社 / 白虎ver<br>
	残り　８日　【後編】松尾大社 / 白虎ver<br>
	残り　７日　【前編】城南宮 / 朱雀ver</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">————————————————-</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>
<br>
<br>

<div id="attachment_691" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-691" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/57cfb879e876023020dde89cfca25744-300x201.png" alt=""
		width="300" height="201">

	<p class="wp-caption-text">ゴホゴホ。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_690" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-690" title="体温計" src="<?php bloginfo('template_directory'); ?>/files/4f9a2c62cdc37388b054b77c13e3ae6d1-300x216.png" alt=""
		width="300" height="216">

	<p class="wp-caption-text">………….実際熱あるっていう。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_692" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-692" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/b19cf2224b34a16a1424e2f35a9fa290-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">………うぅ”。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_693" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-693" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/7628d54ceff4a09a9061776ecd2378be-300x197.png" alt=""
		width="300" height="197">

	<p class="wp-caption-text">染めヒト：おーい！お待たせ熱の人！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_694" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-694" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/c12fe5452e2379b5e5760de5909252d2-300x200.png" alt=""
		width="300" height="200">

	<p class="wp-caption-text">さくっと藍と愛を、吹き込んできたぞ！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_695" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-695" title="衣類渡し" src="<?php bloginfo('template_directory'); ?>/files/05f32d11f9af2ce32db6ef1b60c60f89-300x194.png" alt=""
		width="300" height="194">

	<p class="wp-caption-text">染めヒト：とりあえず、この服を来てくれ。若干、まだ渇いていないが。<br>熱の人：渇いてな…….鬼ですね。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_696" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-696" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/651a29a54fe09c5eeee1cde70ebc8c3e-300x193.png" alt=""
		width="300" height="193">

	<p class="wp-caption-text">熱の人：でもこれで明日の面接までに熱が下がるんなら…冷たっ！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_687" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-687" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/bc2c6a278a8c1d896e4e34389a835eb2-300x196.png" alt=""
		width="300" height="196">

	<p class="wp-caption-text">染めヒト：濡れてるしね！笑<br>かおるさん：染めヒトさん笑わない！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_681" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-681" title="かおるさん" src="<?php bloginfo('template_directory'); ?>/files/24826bf68d30fe68a5fe7b25f50150da-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">染めヒトさん、私から宣伝しましょうか！？</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_682" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-682" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/ce8ee90e27991bc73d0b8830771b8c2b-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">染めヒト：大丈夫。本家がきちんと今回の宣伝をしよう。<br>かおるさん：あ、はい！お願いします！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_683" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-683" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/0fb38c7dda57dbbfbbb8228bc07bab5a-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">熱の人さん、あなた藍染めの衣類を着て、いかがですか？</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_698" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-698" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/465a73eda5132f76f04187cd417dcf98-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">え、あ、まあ色が変わったかなって思います。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_682" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-682" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/ce8ee90e27991bc73d0b8830771b8c2b-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">色が変わった？そうですね。ではあなた、緑の葉っぱからその青い色を作り出したと聞いたら、少しテンションあがりませんか？</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_698" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-698" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/465a73eda5132f76f04187cd417dcf98-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">え、あ、まあ。凄いですね。まあ、若干、微妙にあがりますかね…..</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_683" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-683" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/0fb38c7dda57dbbfbbb8228bc07bab5a-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">
		若干、ですか。なるほど。ではあなた、藍染めが紀元前４５００年以上前のインダス文明からの、７０００年程の歴史を持つと聞いたら、どうでしょう。朱雀が産まれるはるか昔からのその歴史を、今、身にまとっていると聞いたら。</p>
</div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_698" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-698" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/465a73eda5132f76f04187cd417dcf98-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">え、あ、まあ。凄いですね。まあ、ちょっとあがりますかね…..</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_682" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-682" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/ce8ee90e27991bc73d0b8830771b8c2b-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">なるほど、ちょっとですね。わかりました。それではあなた、蒼井優がその衣類を染めるのを手伝ったと聞いたら、いかがですか？</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_673" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-673" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/88ec43c924b90e0e210420db8c4265ba-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">それは凄いテンションあがりますね！！ええ！まじですか！！あの蒼井優ちゃんが！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_116" class="wp-caption aligncenter" style="width: 208px"><img
		class="size-medium wp-image-116" title="蒼井優ちゃん" src="<?php bloginfo('template_directory'); ?>/files/スクリーンショット-2012-09-22-3.23.14-198x300.png" alt=""
		width="198" height="300">

	<p class="wp-caption-text">染めヒト：蒼井優ちゃんは染色するとかぴかぴになってしまうからしないけれども、染色中は片時も離れずにいてくれるんだよ。テンションあがったかい？</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_677" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-677" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/e2d9fe77a28c07296dd319b4c78c2bce-300x195.png" alt=""
		width="300" height="195">

	<p class="wp-caption-text">かぴかぴの意味がちょっとわからないですが、凄いテンション上がりました！マスクいらないです！テンションあがって、熱が下がった感じです！！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_684" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-684" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/e659064e45fdb8056dda099f46a21225-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">テンションが上がると、熱のことなんて全く考えないでしょう！そうです、大事なのはそういうことなのです！マーマー、ゴホン。マーマー！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_681" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-681" title="かおるさん" src="<?php bloginfo('template_directory'); ?>/files/24826bf68d30fe68a5fe7b25f50150da-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">染めヒトさん、そのパターン昨日ジェイさんに先読みされてましたので、やめたほうがいいですよ…。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>

<p style="text-align: center;"><img
		class="aligncenter size-medium wp-image-702" title="ジェイさんのTweet"
		src="<?php bloginfo('template_directory'); ?>/files/268cac49b45affe249e21bb805b0ceb9-211x300.png" alt="" width="211" height="300"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>
<br>
<br>

<div id="attachment_684" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-684" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/e659064e45fdb8056dda099f46a21225-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">………….うん、やめとこう。かおるさん、バトンタッチ。熱の人、もう一回マスクつけてシリアス顔頼む。マーマ……。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_681" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-681" title="かおるさん" src="<?php bloginfo('template_directory'); ?>/files/24826bf68d30fe68a5fe7b25f50150da-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">はい！熱の人さん、あなた藍染めがどんな効用があるのか、知っていますか？</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_698" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-698" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/465a73eda5132f76f04187cd417dcf98-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">いや、知らないです。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_681" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-681" title="かおるさん" src="<?php bloginfo('template_directory'); ?>/files/24826bf68d30fe68a5fe7b25f50150da-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text"><span style="font-size: 12px;">かおるさん：藍は昔から冷え性や肌荒れ、汗もなどに効果があるといわれます！防虫効果も高く、江戸時代の古布も藍で染まった場所だけ虫食いが無かったりしたと言われています！</span>
	</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_706" class="wp-caption aligncenter" style="width: 291px"><img class="size-full wp-image-706"
																				  title="漢方"
																				  src="<?php bloginfo('template_directory'); ?>/files/1569fe342b6c37c3ba8e8867829d80be.png"
																				  alt="" width="281" height="210">

	<p class="wp-caption-text">
		かおるさん：そもそも藍染めで使うタデは染料ではなく、昔は薬としての効果効能を期待されて追求されていました。漢方などの草木が薬として効果があるように、タデもそのように扱われていましたし、現在も発熱にタデの薬を用いる方もおられます。</p>
</div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_703" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-703" title="タデ" src="<?php bloginfo('template_directory'); ?>/files/cdf140c099bad8b6ef043642099e5cb0-300x139.png" alt=""
		width="300" height="139">

	<p class="wp-caption-text">
		かおるさん：タデは、それ自体が薬であり、色を生み出す染料です。熱の人さん、あなたの熱はきっとすぐに治ります。大丈夫です。あなたは薬と、色を吹き込まれた服を着ているんですから、大丈夫です。大丈夫。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_676" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-676" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/bd04a9f8427275aa8ea01bb458039a29-300x195.png" alt=""
		width="300" height="195">

	<p class="wp-caption-text">染めヒト：そうです。かおるさんが言ったことが全てです。大丈夫、あなたの肌は自然にそれを感じ取っていて、もう徐々に熱が下がってきていますよ。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_678" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-678" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/df705f7eb6a916bf8586e3829cd86f66-300x197.png" alt=""
		width="300" height="197">

	<p class="wp-caption-text">染めヒト：それではこのへんで僕はおさらばします。大丈夫、きっとよくなります。大丈夫です。<br>かおるさん：大丈夫です！信じて下さい、あなたは肌はもうすでにそれを感じ取っていますよ！！
	</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_688" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-688" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/d7fd59bc03c536aec72b75b23b0542dc-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">染めヒト：しきたりの一礼をしてと、</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_685" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-685" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/426444a71fdc96941bdea325a003325e-300x202.png" alt=""
		width="300" height="202">

	<p class="wp-caption-text">染めヒト：大丈夫！あなたはもう薬を身にまとっているんだ。大丈夫！<br>かおるさん；大丈夫でーす！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_698" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-698" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/465a73eda5132f76f04187cd417dcf98-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">熱の人：大丈夫？大丈夫なのかな….。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_672" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-672" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/5e0b61f82f11c77103047fd9bd002138-300x195.png" alt=""
		width="300" height="195">

	<p class="wp-caption-text">ん？体がじんじん温かくなってきている気がする…。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_673" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-673" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/88ec43c924b90e0e210420db8c4265ba-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">おい、どんどん体に力がみなぎってきたぞ！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_677" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-677" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/e2d9fe77a28c07296dd319b4c78c2bce-300x195.png" alt=""
		width="300" height="195">

	<p class="wp-caption-text">いける、これならいける！もう熱は無い！大丈夫、あの人が言ってた通り大丈夫！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_679" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-679" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/a28d677d16a903d9c6cb10d95d2cccf4-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">ひゃっほーい！！これで明日の面接もばっちりだ！！効果があったぜーー！大丈夫！大丈夫ーーー！！朱雀より、あの人だーー！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_686" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-686" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/5512d2501a183da3c23970cf2a8f4701-300x195.png" alt=""
		width="300" height="195">

	<p class="wp-caption-text">ひゃっほーーい！アリガトウ！ひゃっほーい！大丈夫だー！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_689" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-689" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/1ea21f8cfe4c9782856164f72fc610c9-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">かおるさん：大丈夫ですかね。。あれでよかったんですかね。。<br>染めヒト：大丈夫だ。なんていったって、きちんと証明されてるんだから。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_699" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-699" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/8feba20570bbb34fe0f8a349233cc82c-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">プラシーボ効果は、絶対だからな。今回もギリギリ、朱雀討伐に関して、赤点は免れたと思うよ。<br>※藍の説明は、本当です。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>

<div id="attachment_689" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-689" title="染めヒト" src="<?php bloginfo('template_directory'); ?>/files/1ea21f8cfe4c9782856164f72fc610c9-300x198.png" alt=""
		width="300" height="198">

	<p class="wp-caption-text">染めヒト：ふう、ようやくあと一体だな。<br>かおるさん；はい、残すは八坂神社の青龍だけです！！</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>

<p style="text-align: center;">………….場所は変わって…………</p>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>
&nbsp;<br>
<br>

<div id="attachment_707" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-707" title="八坂神社" src="<?php bloginfo('template_directory'); ?>/files/30818e16f30f68c89d869bcec2e7fac6-300x197.png" alt=""
		width="300" height="197">

	<p class="wp-caption-text">その頃八坂神社では、一人の小さな女の子がとあるものを探していた……。</p></div>
<br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>
<br>
<img src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
	 alt="" width="658" height="169"><br>

<p style="text-align: center;">最後の一体、青龍を倒す<br>
	to be continue…….</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">———————————–</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">リアルタイムでStory作成、記事の配信中。<br>
	ジェイさん、株式会社LIGの優しさもあって、記事継続中！</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>
<br>
<br>

<div id="attachment_692" class="wp-caption aligncenter" style="width: 310px"><img
		class="size-medium wp-image-692" title="熱の人" src="<?php bloginfo('template_directory'); ?>/files/b19cf2224b34a16a1424e2f35a9fa290-300x199.png" alt=""
		width="300" height="199">

	<p class="wp-caption-text">
		今回早朝雨の中という悪天候の中バイクを走らせ駆けつけてくれ、なおかつ熱が実際に３８℃以上あった熱の人様、誠に有難うございました。染めヒトは素敵な後輩をもてたことを、神様とジェイ様に感謝します。</p></div>
<br>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">
	※株式会社LIGの皆様。勝手に掲載しております。もし削除依頼があれば、すぐに削除させていただきますので、ご一報をお願い致します。勝手に、大変申し訳有りません。Jさん、本当にすみません。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;"><a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/"><img title="Ai for Japs"
																			src="<?php bloginfo('template_directory'); ?>/files/6b7307149d317a5ee0aa3ef496d160e1-300x150.png"
																			alt="" width="300" height="150"></a></p>

<p style="text-align: center;">京都の小さな手染め工房 Japan ○ Colorsでは、皆様の衣類に藍を吹き込む<a

		title="Ai for Japs" href="http://ai-for-japs.boy.jp/somehito/">Ai for Japs</a>というサービスをリリースいたしました。</p>

<p style="text-align: center;"><img
		src="<?php bloginfo('template_directory'); ?>/files/c44215a0f48202340de67c76f620083b.png"
		alt="" width="658" height="169"></p>

<p style="text-align: center;">
	城南宮は大変素晴らしい宮です。ドラえもんや一寸法師などの時代を超えた楽しみももちろんのこと、その古都を印象づけさせる華麗な姿に心をSoっと、豊かにさせてくれます。京都に来た際には皆様、是非城南宮にお越しやす。<br>
	→<a  title="城南宮"
		 href="http://www.jonangu.com/">http://www.jonangu.com/</a></p>
&nbsp;<br>
<br>
&nbsp;<p></p>
</div>
</div>
<!-- END .post -->


<a <?php ga_input(3,'bottom_banner_1'); ?> target="_blank" href="http://liginc.co.jp/author/somehiko"><img src="<?php bloginfo('template_directory'); ?>/images/banner1.jpg" alt="banner1" /></a>
<a <?php ga_input(4,'bottom_banner_2'); ?> target="_blank" href="http://liginc.co.jp/author/shinsan"><img src="<?php bloginfo('template_directory'); ?>/images/banner2.jpg" alt="banner2" /></a>

</div>
<!-- END #left_col -->


</div>
<!-- END #main_content -->


</div>
<!-- END #content -->

<div id="footer_wrap">
	<div id="footer">


		<ul id="copyright" class="cf">
			<li>Copyright ©&nbsp; Somehiko ○ Nikibi</li>
		</ul>

	</div>
	<!-- END #footer -->
</div>


<!-- GA EVENT SCRIPT -->

<?php if(!empty($ga_banners)):?>
	<script>
		$(function(){
			<?php foreach($ga_banners as $v) : ?>
			ga('send', {
				'hitType': 'event',
				'eventAction': 'PageView',
				'eventCategory': '<?php echo $v['bn_cat']; ?>',
				'eventLabel': '<?php echo $v['bn_label']; ?>'
			});
			<?php endforeach;?>

			<?php foreach($ga_banners as $v) : ?>
			$('<?php echo '#'.$v['bn_cat']; ?>').on('click' , function(e){
				ga('send', {
					'hitType': 'event',
					'eventAction': 'Click',
					'eventCategory': '<?php echo $v['bn_cat']; ?>',
					'eventLabel': '<?php echo $v['bn_label']; ?>'
				});
			});
			<?php endforeach; ?>
		});
	</script>
<?php endif; ?>

</body>
</html>

<?php
/**
 * 出力されるバナーをGAのアクション用に記録
 * @param $id
 * @param $cat
 */
function ga_input($id,$cat){
	global $ga_banners;
	$ga_banner['bn_cat'] = $cat;
	$ga_banner['bn_label'] = 'Bn_'.$id;
	$ga_banners[] = $ga_banner;
	echo 'id="' . $cat . '"';
}
