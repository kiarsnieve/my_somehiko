<?php
/**
 * 固定ページ
 *
 * @package WordPress
 */
?>

<?php get_header();?>

<?php
$banners = get_posts(array(
	'post_type' => 'banner',
	'post_status' => 'publish'
));
?>
<?php foreach($banners as $banner): ?>
	<?php
	$image_id = get_field("banner_image",$banner->ID);
	$image = wp_get_attachment_image_src($image_id,"banner");
	?>
	<li>
		<a target="_blank" href="<?php the_field("url",$banner->ID); ?>">
			<img src="<?php echo $image[0]; ?>" height="<?php echo $image[2]; ?>" width="<?php echo $image[1]; ?>">
		</a>
	</li>

<?php endforeach; ?>

<?php get_sidebar()?>
<?php get_footer();?>