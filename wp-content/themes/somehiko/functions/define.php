<?php
/**
 *	定数:定数はここに全部書いてください
 *  定数名は単語毎「_」区切りで全て大文字にすること
 *  使用方法のコメントを必ず残すこと
 *
 *   @author  zuya@LIG.inc
 *   @create  2013/09/12
 *   @version    1.0
 */

/** TOPページの投稿取得件数 */
//define("TOP_POST_LIMIT", "999");