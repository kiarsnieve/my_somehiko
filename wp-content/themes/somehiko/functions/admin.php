<?php
//■■■■■■■■■■■■■■■投稿画面カスタマイズ START■■■■■■■■■■■■■■■
/**
 * アイキャッチの説明を追加する場合に利用します。
 *
 * @param  string $content
 * @return string
 * @author Kobayashi
 */
function lig_wp_add_featured_image_instruction( $content ) {
	global $post_type; // 投稿タイプで変更可能。
	$content.= '<p>';
	$content.= 'アイキャッチ画像として画像を追加するとサムネイルが表示されるようになります。<br />';
	$content.= 'サイズは横300px、縦200pxになるようにしてください。';
	$content.= '</p>';
	return $content;
}
//add_filter( 'admin_post_thumbnail_html', 'lig_wp_add_featured_image_instruction');

/**
 * タイトルのプレースホルダーを変更する場合に利用します。
 *
 * @param  string $title
 * @return string
 * @author Kobayashi
 */
function lig_wp_title_text_input( $title ){
	global $post_type; // 投稿タイプで変更可能。
	return $title = 'ここに記事の題名を書きます';
}
//add_filter( 'enter_title_here', 'lig_wp_title_text_input' );

/**
 * 投稿フォームで不要な項目を除外する場合に利用します。
 *
 * @author Kobayashi
 */
function lig_wp_remove_default_post_screen_metaboxes() {
	remove_meta_box( 'postcustom',      'post','normal' ); // カスタムフィールド
	remove_meta_box( 'postexcerpt',     'post','normal' ); // 抜粋
	remove_meta_box( 'commentstatusdiv','post','normal' ); // コメント
	remove_meta_box( 'trackbacksdiv',   'post','normal' ); // トラックバック
	remove_meta_box( 'slugdiv',         'post','normal' ); // スラッグ
	remove_meta_box( 'authordiv',       'post','normal' ); // 著者
}
//add_action( 'admin_menu','lig_wp_remove_default_post_screen_metaboxes' );

/**
 * 固定ページフォームで不要な項目を除外する場合に利用します。
 *
 * @author Kobayashi
 */
function lig_wp_remove_default_page_screen_metaboxes() {
	remove_meta_box( 'postcustom',      'page','normal' ); // カスタムフィールド
	remove_meta_box( 'commentstatusdiv','page','normal' ); // コメント
	remove_meta_box( 'trackbacksdiv',	'page','normal' ); // トラックバック
	remove_meta_box( 'slugdiv',			'page','normal' ); // スラッグ
	remove_meta_box( 'authordiv',		'page','normal' ); // 著者
}
//add_action( 'admin_menu','lig_wp_remove_default_page_screen_metaboxes' );

/**
 * カテゴリーを入れ子にしたときにチェックしたものを上部にまとめる機能を無効にする場合に利用します。
 *
 * @param  array   $args
 * @param  integer $post_id
 * @return array
 * @author Kobayashi
 */
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
	if ( isset( $args['checked_ontop'] ) === false ) {
		$args['checked_ontop'] = false;
	}
	//投稿画面のカテゴリをLIG_Category_Checklistクラスを使って制御したいときはコメントアウト外す
	//	$args['walker'] = new Danda_Category_Checklist();

	return $args;
}
//add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );


/**
 * iframeとiframeで使える属性を指定する
 * @global array $allowedposttags
 * @param type $content
 * @return type
 */
function add_editor_iframe($content){
	global $allowedposttags;

	// iframeとiframeで使える属性を指定する
	$allowedposttags['iframe'] = array('class' => array () , 'src'=>array() , 'width'=>array(),
			'height'=>array() , 'frameborder' => array() , 'scrolling'=>array(),'marginheight'=>array(),
			'marginwidth'=>array());

	return $content;
}
//add_filter('content_save_pre','add_editor_iframe');

/**
 * 投稿画面のプレビューボタンを非表示にする
 * @global type $post_type
 */
function remove_previewbutton_css_custom() {
	global $post_type; // 投稿タイプで変更可能。
	echo '<style>#preview-action {display: none;}</style>';
}
//add_action('admin_print_styles', 'remove_previewbutton_css_custom');

/**
 * 投稿画面のパーマリンクを非表示にする
 * @global type $post_type
 */
function remove_permlink_css_custom() {
	global $post_type; // 投稿タイプで変更可能。
	echo '<style>#edit-slug-box,#view-post-btn {display: none;}</style>';
}
//add_action('admin_print_styles', 'remove_permlink_css_custom');
/**
 * Walker_Category_Checklistを使って投稿画面のカテゴリをカスタマイズしたい時に使う。
 * 記載の内容はラジオボタン化処理が書いてあります。
 */
require_once(ABSPATH . '/wp-admin/includes/template.php');
class LIG_Category_Checklist extends Walker_Category_Checklist{

	function start_el( &$output, $category, $depth, $args, $id = 0 ) {
		extract($args);
		if ( empty($taxonomy) )
			$taxonomy = 'category';

		if ( $taxonomy == 'category' )
			$name = 'post_category';
		else
			$name = 'tax_input['.$taxonomy.']';

		$class = in_array( $category->term_id, $popular_cats ) ? ' class="popular-category"' : '';

		    $output .= "\n<li id='{$taxonomy}-{$category->term_id}'$class>" . '<label class="selectit"><input value="' . $category->term_id . '" type="radio" name="'.$name.'[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' . checked( in_array( $category->term_id, $selected_cats ), true, false ) . disabled( empty( $args['disabled'] ), false, false ) . ' /> ' . esc_html( apply_filters('the_category', $category->name )) . '</label>';
	}
}

//■■■■■■■■■■■■■■■投稿画面カスタマイズ END■■■■■■■■■■■■■■■

//■■■■■■■■■■■■■■■管理画面外観カスタマイズ START■■■■■■■■■■■■■■■

/**
 * 不要なメニューを削除する場合に利用します。
 *
 * @author Kobayashi
 */
function remove_menus () {
	// global $menu を出力するとslugが確認しやすいです。
	//remove_menu_page( 'edit.php' );
	//remove_menu_page( 'upload.php' );
	//remove_menu_page( 'link-manager.php' );
	//remove_menu_page( 'edit-comments.php' );
	//remove_menu_page( 'tools.php' );
	//remove_menu_page( 'ps-taxonomy-expander.php' );
	//remove_submenu_page( 'index.php', 'update-core.php' ); // 本体更新ページ
	//remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );
	//remove_menu_page('edit.php?post_type=acf');//Advanced Custom Fields
}
if ( ! is_super_admin() ) { // ※管理者以外のログイン時にフックする場合。
	//add_action( 'admin_menu', 'remove_menus' );
}

/**
 * ログイン画面のスタイルシートを変更する場合に利用します。
 *
 * @author Kobayashi
 */
function lig_wp_custom_login() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/css/custom-login-page.css" />';
}
//add_action( 'login_head', 'lig_wp_custom_login' );

/**
 * 管理画面のフッターを変更する場合に利用します。
 *
 * @author Kobayashi
 */
function lig_wp_custom_admin_footer() {
	echo ' <a href="http://liginc.co.jp">株式会社LIG</a>';
}
//add_filter( 'admin_footer_text', 'lig_wp_custom_admin_footer' );

/**
 * ログインメッセージを変更する場合に利用します。
 *
 * @param  object $wp_admin_bar
 * @author Kobayashi
 */
function lig_wp_replace_hello_text( $wp_admin_bar ) {
	$my_account = $wp_admin_bar->get_node( 'my-account' );
	$newtitle = str_replace( 'こんにちは、', 'お疲れさまです！', $my_account->title );
	$wp_admin_bar->add_menu( array(
			'id' => 'my-account',
			'title' => $newtitle,
			'meta' => false
	) );
}
//add_filter( 'admin_bar_menu', 'lig_wp_replace_hello_text', 25 );


/**
 * WordPressSEOプラグインのメニュー非表示
 */
function remove_wordpress_seo_menu() {
	echo '<style type="text/css">';
	echo '#toplevel_page_wpseo_dashboard,#wp-admin-bar-view,#wp-admin-bar-wpseo-menu {';
	echo '  display: none;';
	echo '}';
	echo '</style>';
}
//add_action( 'admin_head', 'remove_wordpress_seo_menu', 100);

// バージョンを表示しない
remove_action('wp_head', 'wp_generator');
//■■■■■■■■■■■■■■■管理画面外観カスタマイズ END■■■■■■■■■■■■■■■


//■■■■■■■■■■■■■■■投稿一覧、カテゴリ一覧カスタマイズ START■■■■■■■■■■■■■■■

/**
 * 投稿一覧画面で表示タグを非表示にする（CSS）
 *
 * @author Kobayashi
 */
function remove_indexpage_view_link() {
	global $post_type; // 投稿タイプで変更可能。
	echo '<style type="text/css">';
	echo 'span.view {';
	echo '  display: none;';
	echo '}';
	echo '</style>';
}
//add_action('admin_print_styles-edit.php', 'remove_indexpage_view_link');

/**
 * カテゴリ一覧で表示リンクを非表示にする
 */
function remove_category_view_link() {
	echo '<style type="text/css">';
	echo '.view {';
	echo '  display: none;';
	echo '}';
	echo '</style>';
}
//add_action("admin_head-edit-tags.php", "remove_category_view_link");

//■■■■■■■■■■■■■■■投稿一覧、カテゴリ一覧カスタマイズ END■■■■■■■■■■■■■■■

/**
 * 固定ページなどでURLを使用する際のショートコードを設定します
 * @param type $atts
 * @return type
 */
function my_home_url( $atts ) {
	return home_url();
}
//add_shortcode( 'my_home_url', 'my_home_url' );


/**
 * カテゴリ一覧で表示リンクを非表示にする
 */

//カテゴリ一覧ページの説明の箇所を非表示にする
if(is_admin()){
	if($pagenow == "edit-tags.php"){
		function remove_tag_view_link(){
			echo '<style type="text/css">';
			echo '.form-field:nth-of-type(4){';
			echo '  display: none;';
			echo '}';
			echo '</style>';

		}
		add_action("admin_print_styles-edit-tags.php", "remove_tag_view_link");
	}
}


/**
 * タイトルが入力されていない場合、alertを出します
 */
// add_action( 'admin_head-post-new.php', 'my_title_required' );
function my_title_required() {
	?>
<script type="text/javascript">
jQuery(document).ready(function($){
	if('post' == $('#post_type').val()){
		$("#post").submit(function(e){
			if('' == $('#title').val()) {
				alert('タイトルを入力してください！');
				$('#ajax-loading').css('visibility', 'hidden');
				$('#publish').removeClass('button-primary-disabled');
				$('#publishing-action .spinner').hide();
				$('#title').focus();
				return false;
			}
		});
	}
});
</script>
<?php
}

/**
 * カテゴリが入力されていない場合、alertを出します
 */
if ( !has_action( 'admin_footer', 'alert_category' ) ){
	add_action( 'admin_footer' , 'alert_category' );
}
function alert_category() {
	echo <<< EOF
<script type="text/javascript">

	jQuery("#post").attr("onsubmit", "return check_category();");

	function check_category(){
		if(jQuery("#categorychecklist").length) {
			var check_num = jQuery("#categorychecklist input:checked").length;
			if(check_num <= 0){
				alert("カテゴリを選択してください。");
				jQuery("#ajax-loading").css("visibility","hidden");
				jQuery("#publish").removeClass("button-primary-disabled");
				return false;
			} else {
				return true;
			}
		}
	}

</script>';
EOF;
}
