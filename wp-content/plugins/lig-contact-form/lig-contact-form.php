<?php
session_start();
/**
Plugin Name: LIG Contact Form
Description: contact form plugin.
Author: zuya@LIG
Version: 0.8a
*/

add_shortcode( 'contact-form', 'contact_form' );

function contact_form($attr){

	extract(shortcode_atts(array(
		'form_class' => 'ContactForm',
	), $attr));

	define("LIB_PATH", dirname(realpath(__FILE__)) . '/Lib/');
	require_once(LIB_PATH . "Contact.class.php");

	$Form = new $form_class;
	ob_start();
	echo $Form->render();
	$out = ob_get_contents();
	ob_end_clean();
	return $out;
}