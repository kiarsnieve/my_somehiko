<?php
Class Config {
    //テンプレート設定
    var $template = array(
        'form'    => 'Templates/form.php',
        'confirm' => 'Templates/confirm.php',
        'comp'    => 'Templates/comp.php',
    );
	
	//ページタイトル設定
	var $pageTitle = array(
		'form'    => 'お申込み画面',
		'confirm' => '確認画面',
		'comp'    => '完了画面',
	);

    //オプション設定
    var $option = array(
        //'redirect'=> 'http://google.com', // 送信完了画面を表示せずにリダイレクトさせる場合に指定
        'errorColor' => '#FBEFF3',
        'errorClass' => 'inputError',
        'error-msg'  => '※入力内容に漏れがあります。下記フォームをご確認の上再度送信するボタンをクリックしてください。',
    );
	
    //メール設定
    var $email = array(
        //送信者宛
        array(
            'to'        =>'email',
            'to_name'   =>'name',
            'from'      =>'kobayashi@liginc.co.jp',
            'from_name' =>'',
            'subject'   =>'【自動返信メール】',
            'template'   => 'Templates/mail_tpl2.php',
        ),
        //管理用
        array(
            'to'      =>'kobayashi@liginc.co.jp',
            'to_name'   =>'',
            'from'      =>'email',
            'from_name' =>'name',
            'subject'   =>'【自動返信メール】',
            'template'   => 'Templates/mail_tpl3.php',
        ),
    );
	
    //入力チェック関連
    var $validate = array(
        'nama' => array(
            'requid' => array('msg'=>'<strong>お名前</strong>は必須項目です'),
        ),
        'kana' => array(
            'kana'   => array('msg'=>'<strong>ふりがな</strong>は半角カタカナで入力してください'),
            'requid' => array('msg'=>'<strong>ふりがな</strong>必須項目です'),
        ),
        'email' => array(
            'email'  => array('msg'=>'<strong>メールアドレス</strong>はuser@exsmple.com形式の半角英数で入力してください。'),
            'requid' => array('msg'=>'<strong>メールアドレス</strong>は必須項目です'),
        ),
        'tel' => array(
            'phone' => array('msg'=>'<strong>電話番号</strong>は正しい形式で入力してください'),
        ),
        'message' => array(
            'requid' => array('msg'=>'<strong>コメント</strong>は必須項目です'),
        ),
        'policy' => array(
            'requid' => array('msg'=>'<strong>プライバシーポリシー</strong>に同意してください'),
        ),
    );
}
?>