			<div id="one_column">
				<section id="contact">
					<h1><img src="<?php bloginfo('template_directory'); ?>/images/pages/ttl_contact.png" width="305" height="29" alt="お問い合わせ"></h1>
					<form method="POST" action="?" name="form" id="contact_form">
						<table>
							<tr>
								<th>お問い合わせ種別<span>必須</span></th>
								<td>
									<?php echo $this->config->contact_type[xss($this->data['contact_type'])];?><br><br>
									<?php echo $this->config->contact_group[xss($this->data['contact_group'])];?>
								</td>
							</tr>
							<tr>
								<th>貴社名</th>
								<td><?php echo xss($this->data['company_name'])?></td>
							</tr>
							<tr>
								<th>役職</th>
								<td><?php echo xss($this->data['position'])?></td>
							</tr>
							<tr>
								<th>お名前<span>必須</span></th>
								<td><?php echo xss($this->data['name'])?></td>
							</tr>
							<tr>
								<th>お名前（フリガナ）<span>必須</span></th>
								<td><?php echo xss($this->data['kana'])?></td>
							</tr>
							<tr>
								<th>郵便番号</th>
								<td><?php echo xss($this->data['zipcode'])?></td>
							</tr>
							<tr>
								<th>ご住所</th>
								<td><?php echo xss($this->data['add'])?></td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td><?php echo xss($this->data['tel'])?></td>
							</tr>
							<tr>
								<th>FAX番号</th>
								<td><?php echo xss($this->data['fax'])?></td>
							</tr>
							<tr>
								<th>メールアドレス<span>必須</span></th>
								<td><?php echo xss($this->data['email'])?></td>
							</tr>
							<tr>
								<th>お問い合わせ内容<span>必須</span></th>
								<td><?php echo nl2br(xss($this->data['biko']))?></td>
							</tr>
						</table>
						<div class="sub_box2">
							<input type="submit" name="back" value="入力内容を修正する" class="submit_style02" id="back_link">
							<input type="submit" name="post" value="入力内容で送信する" class="submit_style01">
						</div>

						<input type="hidden" name="_param[mode]" value="comp" id="hidden_mode">
						<?php foreach( $this->data as $key => $value ): ?>
						<input type="hidden" name="data[<?php echo $key; ?>]" value="<?php echo xss($value); ?>" />
						<?php endforeach; ?>
					</form>
				</section>
			</div>
/**
//前の画面に戻るをJSでやる場合
<script>
		$(function(){
			$("#back_link").on("click",function(){//入録画面に戻るボタンのセレクタを指定する
				$("#hidden_mode").val("back");//hiddenのnameがmodeの値を戻る用の値に変える
				$('#contact_form').submit();//フォームタグのセレクタを指定してsubmitする
			});
		});
</script>
**/