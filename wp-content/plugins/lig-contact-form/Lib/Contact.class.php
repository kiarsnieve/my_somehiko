<?php
require_once LIB_PATH . 'Config.class.php';
require_once LIB_PATH . 'Form.class.php';
class ContactConfig extends Config {
	var $template = array(
			'form'    => 'Templates/Contact/form.php',
			'confirm' => 'Templates/Contact/confirm.php',
			'comp'    => 'Templates/Contact/comp.php',
	);

	//メール設定
    var $email = array(
        //送信者宛
        array(
            'to'        => 'email',
            'to_name'   => 'name',
            //'from'      => 'info@six-star.net',
        	'from'      => 'takato@liginc.co.jp',
            'from_name' => '株式会社サンプル',
            'subject'   => '【株式会社サンプル】お問い合わせ頂きありがとうございます',
            'template'  => 'Templates/Contact/mail_tpl1.php',
        	'file' => false
        ),
        //管理用
        array(
            //'to'        => 'info@six-star.net',
    		'to' => 'takato@liginc.co.jp' ,
            'to_name'   => '株式会社サンプル',
            'from'      => 'email',
            'from_name' => 'name',
            'subject'   => '【株式会社サンプル】お問い合わせがありました',
            'template'  => 'Templates/Contact/mail_tpl2.php',
			'log_level' => 0, // 1 or 2 or 3 or 0
        ),
    );

	// 半角自動変換
    var $convertKana = array( 'tel,fax,email,zipcode' => 'a' , 'kana' => 'K');

    var $contact_type = array(
    	'1' => 'ADinsightに関するお問い合わせ' ,
    	'2' => 'TapBoardに関するお問い合わせ' ,
    	'3' => 'A.I.ADに関するお問い合わせ' ,
    	'4' => 'その他のサービスに関するお問い合わせ' ,
    	'5' => '採用に関するお問い合わせ' ,
    	'6' => 'その他に関するお問い合わせ'
    );

    var $contact_group = array(
    	'1' => 'お問い合わせ' ,
    	'2' => '資料請求' ,
    );


    //入力チェック関連
    var $validate = array(
    	'contact_type' => array(
    		'required' => array('msg'=>'<strong>お問い合わせ種別</strong>を選択してください。'),
    	),
    	'contact_group' => array(
    		'required' => array('msg'=>'<strong>お問い合わせ種別</strong>を選択してください。'),
    	),
		'name' => array(
			'required' => array('msg'=>'<strong>お名前</strong>は必須項目です。'),
		),
    	'kana' => array(
    		'required' => array('msg'=>'<strong>お名前(フリガナ)</strong>は必須項目です。'),
    	),
		'email' => array(
			'required' => array( 'msg' => '<strong>メールアドレス</strong>は必須項目です。' ),
			'email'	   => array( 'msg' => '<strong>メールアドレス</strong>は正しい形式で入力してください。' ),
		),
		'zipcode' => array(
			'hyphennum' => array('msg'=>'<strong>郵便番号</strong>は数字で入力してください。'),
		),
		'tel' => array(
			'hyphennum' => array('msg'=>'<strong>電話番号</strong>は数字で入力してください。'),
		),
		'fax' => array(
			'hyphennum' => array( 'msg' => '<strong>FAX番号</strong>は数字で入力してください。' ),
		),
		'biko' => array(
			'required' => array('msg'=>'<strong>お問い合わせ内容は</strong>は必須項目です。'),
		),
    	'agree' => array(
    		'required' => array('msg'=>'<strong>個人情報の取り扱い</strong>について同意が必要です。'),
    	),
    );

}
class ContactForm extends Form  {

	var $id = "contact";

	function loadConfig() {

		//postデータの加工
		if(!empty($_POST['tel'])) $_POST['tel'] = str_replace("ー", "-", $_POST['tel']);
		if(!empty($_POST['zipcode'])) $_POST['zipcode'] = str_replace("-ー", "", $_POST['zipcode']);

		$this->config = new ContactConfig($this->mode);
	}

}


?>