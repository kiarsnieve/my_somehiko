<?php
//Validation
require_once LIB_PATH . 'Validation.php';
//Functions
require_once LIB_PATH . 'functions.php';
//Config
require_once LIB_PATH . 'Config.class.php';
//Qdmail
require_once LIB_PATH . 'qdmail.php';

if(!defined("TMP_FILE_PATH")) define('TMP_FILE_PATH', dirname(__FILE__) . '/tmp/');
if(!defined("MAX_FILE_SIZE")) define('MAX_FILE_SIZE' , '2097152');
if(!defined("LOG_FILE_PATH")) define('LOG_FILE_PATH' , dirname(__FILE__) . '/logs/');

Class Form {

	/**
	 * フォームインスタンスの識別子。
	 * SESSIONデータが複数フォームでコンフリクトするのを防ぐため。
	 * ドメイン内でユニークなことがのぞましい。
	 * @var string
	 */
	var $id = 'form';

    /**
     * Configクラスのインスタンス
     */
    var $config  = null;
    var $data    = null;
    var $errors  = array();
    var $mode    = null;
    var $output  = null;
    var $template = null;
    var $title   = null;

    //カスタマイズ変数
    var $message = array();

    function Form() {
        //初期化
        $this->formInit();

        //コントローラー
        switch ($this->mode){
        case 'comp':
            //確認画面からのPOST
        	if(isset($_POST['back']) || isset($_POST['back_x']) && isset($_POST['back_y'])) {
                $this->formForm();
            } else {
                $this->formComp();
            }
            break;
        case 'confirm':
            //入力画面からのPOST
            $this->formConf();
            break;
        case 'form':
            $this->formForm();
            break;
        default:
            $this->formDefault();
            break;
        }
    }

    function loadConfig() {
        $this->config = new Config($this->mode);
    }

    /**
     * 初期化
     *
     * @access private
     */
    function formInit() {

        $val  = trim(ini_get('upload_max_filesize'));
        $last = strtolower($val{strlen($val)-1});
        switch($last) {
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
        }
        if(!defined('MAX_FILE_SIZE')) {
        	define("MAX_FILE_SIZE", $val);
        }

        if(!empty($_POST['_param']['mode'])){
            $this->mode = $_POST['_param']['mode'];
        }
        $this->loadConfig();

        $data = null;

        if(!empty($_POST['data'])){
            foreach($_POST['data'] as $k => $v){
                $data[$k] = trim($v);
            }
            if ( ! empty( $this->config->convertKana ) ) {
                foreach( $this->config->convertKana as $inputs => $case ) {
                    $inputs = explode( ',', $inputs );
                    foreach( $inputs as $input ) {
                        if ( isset( $data[$input] ) && strlen( $data[$input] ) > 0) {
                            $data[$input] = mb_convert_kana( $data[$input], $case, 'UTF-8' );
                        }
                    }
                }
            }
        }

        // ファイルも名前だけここに保存しておく
        if( ! empty( $_FILES ) ) {
            foreach( $_FILES['data']['name'] as $k => $v ) {
                if( ! isset( $data[$k] ) ) {
                    $data[$k] = trim($v);
                }
            }
        }
        //pr($data);

        if( ! empty( $data ) ) {
			// 正常なPOSTデータがあればセッションに保存する
            if( ! empty( $_SESSION[$this->id]['data'] ) ) {
                $_SESSION[$this->id]['data'] = array_merge( $_SESSION[$this->id]['data'], $data );
            }else{
                $_SESSION[$this->id]['data'] = $data;
            }
        }

        //pr($_SESSION['data']);

        if(!empty($this->mode)){
            $this->data = $_SESSION[$this->id]['data'];

            if(!empty($this->config->validate)){
                foreach($this->config->validate as $k => $v){
                    $this->inputValidate($k,$v);
                }
            }
        }else{
            $_SESSION[$this->id]['data'] = null;
            $this->data = $data;
        }
    }


	//バリデート関連
	function validate(){
		//複合チェックなどを処理
		if(!empty($_FILES)){
			// エラーがなくて、ファイルがある場合、一回上げる
			foreach( $_FILES["data"]["name"] as $key => $value ) {

				// エラーがある場合処理しない
				if(!empty($this->errors[$key])) {
					// セッションに保持しているファイルがある場合削除
					if(!empty( $_SESSION[$this->id]['file'][$key]['_file_src'] ) ) {
						if(file_exists(TMP_FILE_PATH . $_SESSION[$this->id]['file'][$key]['_file_src'])) {
							unlink(TMP_FILE_PATH . $_SESSION[$this->id]['file'][$key]['_file_src']);
						}

						unset($_SESSION[$this->id]['file'][$key]['_file_src'] , $_SESSION[$this->id]['file'][$key]['_file_name']);
					}

					continue;
				}

				$tmp_file_name = $value;

				// 拡張子だけ取り出す
				$dsPos = strrpos( $tmp_file_name, '.' );

				$befor = substr( $tmp_file_name, 0, $dsPos );
				$after = substr( $tmp_file_name, $dsPos, strlen( $tmp_file_name ) );
				$tmp_file_name = md5( uniqid( $this->id, true ) ) . $after;

				if( move_uploaded_file( $_FILES['data']['tmp_name'][$key], TMP_FILE_PATH . $tmp_file_name ) ) {

					chmod(TMP_FILE_PATH.$tmp_file_name, 0777);

					// 旧ファイルが保持されている場合、削除
					if( ! empty( $_SESSION[$this->id]['file'][$key]['_file_src'] ) ) {
						if(file_exists(TMP_FILE_PATH . $_SESSION[$this->id]['file'][$key]['_file_src']))
							unlink(TMP_FILE_PATH . $_SESSION[$this->id]['file'][$key]['_file_src']);
					}

					// 成功したらセッションに保持する
					$_SESSION[$this->id]['file'][$key]['_file_src'] = $tmp_file_name;
					$_SESSION[$this->id]['file'][$key]['_file_name'] = $_FILES['data']['name'][$key];
				}

			}
		}

		if(count($this->errors)){
			$this->message['error'] = $this->config->option['error-msg'];
			return false;
		}

		return true;
	}

    function formComp(){
        $this->title = $this->config->pageTitle['comp'];
        //いろいろ処理用。
        if(!$this->validate()){
            $this->title = $this->config->pageTitle['form'];
            $this->template = $this->config->template['form'];
            return;
        }

        foreach($this->config->email as $mail_option){
            $this->sendMail($mail_option);
        }

        // ファイルがある場合、ファイルを消す
        if( ! empty( $_SESSION[$this->id]['file'] ) ) {
			foreach( $_SESSION[$this->id]['file'] as $key => $value ) {
				if( file_exists( TMP_FILE_PATH . $value['_file_src'] ) ) {
					unlink( TMP_FILE_PATH . $value['_file_src'] );
				}
			}
        }

        //セッションを消す
        $_SESSION[$this->id] = null;

        if( ! empty( $this->config->option['redict'] ) )
            header('Location: '.$this->config->option['redict']);

        $this->template = $this->config->template['comp'];

        return;
    }

    function formConf(){
        $this->title = $this->config->pageTitle['confirm'];

        //いろいろ処理用。
        if(!$this->validate()){
            $this->title = $this->config->pageTitle['form'];
            $this->template = $this->config->template['form'];
            return;
        }

        $this->template = $this->config->template['confirm'];
        return;
    }

    function formForm(){
        $this->title = $this->config->pageTitle['form'];

        $this->validate();
        $this->template = $this->config->template['form'];
        return ;
    }

    function formDefault(){
        $this->title = $this->config->pageTitle['form'];

        $this->template = $this->config->template['form'];
        return ;
    }


    //レンダー関連
    function render(){
        $option = $this->config->option;

        ob_start();
        require_once $this->template;

        $this->output = ob_get_contents();
        ob_end_clean();

        echo $this->output;
    }

    //インプット処理関連
    function input($fieldName, $options = array()){
        $output  = null;
        $default = array(
            'type' => 'text',
            'error' => false,
        );

        $options = array_merge($default,$options);

        switch($options['type']){
        case'select':
            $output =  $this->select($fieldName,$options);
            break;

        case'checkbox':
            $output =  $this->checkbox($fieldName,$options);
            break;

        case'radio':
            $output =  $this->radio($fieldName,$options);
            break;

        case'textarea':
            $output =  $this->textarea($fieldName,$options);
            break;

        case'hidden':
            $output =  $this->hidden($fieldName,$options);
            break;
        case 'file':
            $output =  $this->file($fieldName,$options);
            break;
        default:
        case'text':
            $output =  $this->text($fieldName,$options);
            break;
        }

        $error = $this->inputError($fieldName);
        if($options['error']===false)
            $error = null;

        return $output.$error;
    }

    function setMailOption( &$mail_option ) {
        foreach($mail_option as $k => $v){
            if ( is_array($v) ) {
                $str = '';
                foreach( $v as $key ) {
                    if ( !empty($this->data[$key] ) ) {
                        $str.=$this->data[$key];
                    } else {
                        $str.=$key;
                    }
                }
                $mail_option[$k] = $str;
            } else {
                if(!empty($this->data[$v]))
                    $mail_option[$k] = $this->data[$v];
            }
        }
    }

    function sendMail($mail_option){
        $option = $this->config->option;

        foreach($this->data as $k => $v){
            ${$k} = $v;
        }

        $this->setMailOption( $mail_option );

        require $mail_option['template'];
        //pr($option['body']);

        mb_language('Japanese');

        $mail = new Qdmail();

		if ( isset( $mail_option['log_level'] ) ) {
			$mail->logLevel( $mail_option['log_level'] );
			$mail->logPath( LOG_FILE_PATH );
			$mail->logFilename( 'qdmail_' . date('Ym') . '.log' );
		}

        $mail->to( $mail_option['to'], $mail_option['to_name'] );
        $mail->from( $mail_option['from'] , $mail_option['from_name'] );
        $mail->subject( $mail_option['subject'] );
        $mail->text( $mail_option['body'] );

		if( isset( $mail_option['bcc'] ) ) {
			$mail->bcc( $mail_option['bcc'] );
		}

        // セッションにファイルがある場合、添付ファイルを添付
        if( (isset($mail_option['file']) && $mail_option['file'] == true) && !empty( $_SESSION[$this->id]['file'] ) ) {
			$attach = array();
			foreach( $_SESSION[$this->id]['file'] as $key => $value ) {
				$attach[] = array( TMP_FILE_PATH . $value['_file_src'], $value['_file_name'] );
			}
			if ( $attach ) {
				$mail->attach(  $attach );
			}
        }

        $mail->lineFeed("\n");
        $mail->send();
    }

    function inputInit($fieldName,$options = array()){

        if(!empty($options['empty'])){
            $inputOption = array(""=>$options['empty']);
            foreach($options['option'] as $k => $v){
                $inputOption[$k] = $v;
            }
            $options['option'] = $inputOption;
            //array_unshift($options['option'],$options['empty']);
            unset($options['empty']);
        }

        //pr($options);

        if(!empty($this->errors[$fieldName]) && count($this->errors[$fieldName])){
            if(!empty($options['style'])){
                $options['style'] .= 'background-color: '.$this->config->option['errorColor'].';';
            }else{
                $options['style'] = 'background-color: '.$this->config->option['errorColor'].';';
            }
        }

        if($options['type']!='checkbox'
           && $options['type']!='select'
           && $options['type']!='radio'){
            if(!empty($options['default']) && empty($options['value'])){
                $options['value'] = $options['default'];
                unset($options['default']);
            }
            if(isset($this->data[$fieldName])  && mb_strlen($this->data[$fieldName]) > 0){
                $options['value'] = $this->data[$fieldName];
            }
        }

        unset($options['error']);

        return $options;
    }

    function inputAttr($options = array()){
    	$attr = array();
        foreach($options as $k => $v){
            if($k == 'option'
               || $k == 'required'
               || $k == 'error'
               || $k == 'type')
                continue;
            $attr[] = $k.'="'.stripslashes(xss($v)).'"';
        }
        return $attr;
    }

    /**
     * メインバリデーション
     *
     * @param mixed $fieldName
     * @param
     * @return void
     */
    function inputValidate( $fieldName, $validate ){

        $error = array();

        foreach ( $validate as $rule => $args) {
            if ( method_exists( $this, $rule ) ) {
                $errorMessage = '';
                if ( isset($args['msg']) ) {
                    $errorMessage = $args['msg'];
                    unset($args['msg']);
                }
                if ( ! call_user_func_array( array( $this, $rule), $args ) ) {
                    $error[] = $errorMessage;
                }
            }
        }
        if(!empty($this->data[$fieldName])){
            $data = $this->data[$fieldName];
            if(!empty($validate['num'])){
                if(!ereg("^[0-9]+$",$data))
                    $error[] = $validate['num']['msg'];
            }

            if(!empty($validate['hyphennum'])){
            	if(!preg_match("/^[-0-9]*$/",$data))
            		$error[] = $validate['hyphennum']['msg'];
            }

            if(!empty($validate['email'])){
                if(!eregi("^[_a-z0-9\._-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$data))
                    $error[] = $validate['email']['msg'];
            }

            if(!empty($validate['post'])){
                if(!eregi("^[0-9]{1,3}\-[0-9]{1,4}$",$data))
                    $error[] = $validate['post']['msg'];
            }

            if(!empty($validate['phone'])){
                if(!eregi("^[0-9]{1,4}\-{0,1}[0-9]{1,4}\-{0,1}[0-9]{1,4}$",$data))
                    $error[] = $validate['phone']['msg'];
            }

            if(!empty($validate['kana'])){
                if(!preg_match("/^[ァ-ヶー　]+$/u",$data))
                    $error[] = $validate['kana']['msg'];
            }

            if(!empty($validate['equ'][0])){
                if(mb_strlen($data) != $validate['equ'][0])
                    $error[] = $validate['equ'][0].$validate['equ']['msg'];
            }

            if(!empty($validate['max'][0])){
                if(mb_strlen($data) >  $validate['max'][0])
                    $error[] = $validate['max'][0].$validate['max']['msg'];
            }

            if(!empty($validate['min'][0])){
                if(mb_strlen($data) <  $validate['min'][0])
                    $error[] = $validate['min'][0].$validate['min']['msg'];
            }

            if(!empty($validate['exe']) && isset($_FILES['data'])){
                // 別 拡張子のチェック
                if(!preg_match("/.(pdf|xls|xlsx|doc|docx)$/", strtolower($_FILES['data']['name'][$fieldName]))){
                    $error[] = $validate['exe']['msg'];
                }
            }

            if(!empty($validate['max_file_size']) && isset($_FILES['data'])){
                if(MAX_FILE_SIZE <= $_FILES['data']['size'][$fieldName]) {
                    $error[] = $validate['max_file_size']['msg'];
                }
            }

        }else{
            $data = $this->data[$fieldName];
            if(!empty($validate['required']) && !(isset($this->data[$fieldName]) && mb_strlen($this->data[$fieldName]) > 0))
                $error[] = $validate['required']['msg'];
        }

        if(count($error))
            $this->errors[$fieldName] = $error;
    }

    function inputError($fieldName){
        $error = array();
        $ret = '';

        if(is_array($fieldName)){
            foreach($fieldName as $v){
                if(!empty($this->errors[$v])){
                    $error = $this->errors[$v];
                }
            }
        }else{
            if(!empty($this->errors[$fieldName])){
                $error = $this->errors[$fieldName];
            }
        }

        $error = array_unique($error);

        if($error){
            if(!empty($this->config->option['errorClass'])){
                $class = ' class="'.$this->config->option['errorClass'].'"';
            }

            $ret  = '<ul'.$class.'>';
            foreach($error as $v){
                $ret .= '<li>'.$v.'</li>';
            }
            $ret .= '</ul>';
        }

        return $ret;
    }

    function checkbox($fieldName,$options = array()){
    	$options['type'] = "checkbox";
    	$options = $this->inputInit($fieldName,$options);

        $label = null;
        if(!empty($options['label'])){
            $label = $options['label'];
            unset($options['label']);
        }
        if(!empty($options['value']) && !empty($this->data[$fieldName])){
            if($this->data[$fieldName] == $options['value']){
                $options['checked'] = 'checked';
            }
        }
        $attr = $this->inputAttr($options);

        return '<input type="checkbox" name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'>';
    }

    function radio($fieldName,$options = array()){
    	$options['type'] = "radio";
    	$options = $this->inputInit($fieldName,$options);

        $label = null;
        if(!empty($options['label'])){
            $label = $options['label'];
            unset($options['label']);
        }
        if(!empty($options['value']) && !empty($this->data[$fieldName])){
            if($this->data[$fieldName] == $options['value']){
                $options['checked'] = 'checked';
            }
        }
        $attr = $this->inputAttr($options);

        return '<input type="radio" name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'>';
    }

    function select($fieldName,$options = array()){
        $options = $this->inputInit($fieldName,$options);
        $attr = $this->inputAttr($options);

        $ret = '<select name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'>';
        if(!empty($options['option'])){
            foreach($options['option'] as $k => $v){
				if ( is_array( $v ) ) {
					$ret.= '<optgroup label="'. xss($k) .'">';
					foreach($v as $__k => $__v){
						$selected = '';
						if(isset($this->data[$fieldName]) && $__k == $this->data[$fieldName])
							$selected = ' selected="selected"';
						$ret .= '<option value="'.xss($__k).'"'.$selected.'>'.xss($__v).'</option>';
					}
					$ret.= '</optgroup>';
				} else {
					$selected = '';
					if(isset($this->data[$fieldName]) && $k == $this->data[$fieldName])
						$selected = ' selected="selected"';

					$ret .= '<option value="'.xss($k).'"'.$selected.'>'.xss($v).'</option>';
				}
            }
        }
        $ret .= '</select>';

        return $ret;
    }

    function textarea($fieldName,$options = array()){
        $options = $this->inputInit($fieldName,$options);
        $value = null;
        if(!empty($options['value'])){
            $value = $options['value'];
            unset($options['value']);
        }
        $attr = $this->inputAttr($options);

        return '<textarea name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'>'.$value.'</textarea>';
    }

    function text($fieldName,$options = array()){

        $options = $this->inputInit($fieldName,$options);

        $attr = $this->inputAttr($options);

        return '<input type="text" name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'/>';
    }

    function file($fieldName,$options = array()){

        $options = $this->inputInit($fieldName,$options);

        $attr = $this->inputAttr($options);

        unset($attr['value']);

        // セッションにファイルがある場合、ファイル名も表示してあげる
        $file = "";

        if(!empty($_SESSION[$this->id]['file'][$fieldName]['_file_name'])) {
            $file = $_SESSION[$this->id]['file'][$fieldName]['_file_name'].'ファイルを添付済<br /><span style="color:red">※ファイル名に日本語が含まれている場合正常に表示されない場合が御座いますが、添付自体は問題なく行えています。</span><br />';
        }

        return $file.'<input type="file" name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'/>';
    }

    function hidden($fieldName,$options = array()){
        $options = $this->inputInit($fieldName,$options);

        if(isset($options['hidden-value'])){
            $options['value'] = $options['hidden-value'];
            unset($options['hidden-value']);
        }

        $attr = $this->inputAttr($options);

        return '<input type="hidden" name="data['.xss($fieldName).']"'.($attr ? ' '.join(' ',$attr) : '').'/>';
    }

    /**
     * いずれかのフィールドが空でなければ真を返します。
     *
     * @return boolean
     */
    function checkGroupRequired() {
    	$args = func_get_args();
    	foreach( $args as $arg ) {
    		if ( isset( $this->data[$arg] ) ) {
    			if ( strlen( $this->data[$arg] ) > 0 ) return true;
    		}
    	}
    	return false;
    }

	/**
	 * いずれのフィールドも空でなければ真を返します。
	 *
	 * @return boolean
	 */
	function checkGroupNotEmpty() {
		$args = func_get_args();
        foreach( $args as $arg ) {
            if ( isset( $this->data[$arg] ) ) {
                if ( strlen( $this->data[$arg] ) === 0 ) return false;
            }
        }
        return true;
	}

	/**
	 * いずれのフィールドも全角カナで構成されていれば真を返します。
	 *
	 * @return boolean
	 */
	function checkGroupKana() {
        $args = func_get_args();
        foreach( $args as $arg ) {
            if ( isset( $this->data[$arg] ) ) {
                if(!preg_match("/^[ァ-ヶー　]+$/u",$this->data[$arg]))
                    return false;
            }
        }
        return true;
    }

    /**
     * いずれのフィールドも半角数字で構成されていれば真を返します。
     *
     * @return boolean
     */
    function checkGroupNum() {
    	$args = func_get_args();
    	foreach( $args as $arg ) {
    		if ( isset( $this->data[$arg] ) && mb_strlen($this->data[$arg]) > 0) {
    			if(!preg_match("/^[0-9]+$/u",$this->data[$arg]))
    				return false;
    		}
    	}
    	return true;
    }

    /**
     * どれかひとつ入力されていたら、すべて必須にする
     */
    function checkGroupIfNotEmpty(){
    	$args = func_get_args();

    	$isNotEmpty = false;
    	foreach( $args as $arg ) {
    		if ( isset( $this->data[$arg] ) && mb_strlen($this->data[$arg]) > 0) {
    			$isNotEmpty = true;
    			break;
    		}
    	}

    	if(!$isNotEmpty) {
    		return true;
    	}

    	foreach( $args as $arg ) {
    		if ( isset( $this->data[$arg] ) && mb_strlen($this->data[$arg]) == 0) {
    			return false;
    		}
    	}

    	return true;
    }

	/**
	 * Compare values.
	 *
	 * @use Validation
	 * @see Validation::equalTo()
	 */
	function checkCompare() {
		$args = func_get_args();
		return Validation::equalTo( $this->data[$args[0]], $this->data[$args[1]] );
	}

/**
     * いずれのフィールドも半角数字で1～4桁で構成されていれば真を返します。
	 * 電話番号を項目分けしている時に使用
     *
     * @return boolean
     */
    function checkGroupPhoneNum() {
    	$args = func_get_args();
    	foreach( $args as $arg ) {
    		if ( isset( $this->data[$arg] ) && mb_strlen($this->data[$arg]) > 0) {
    			if(!preg_match("/^[0-9]{1,4}+$/u",$this->data[$arg]))
    				return false;
    		}
    	}
    	return true;
    }
	
	/**
     * どれかひとつ入力されていたら、すべて必須にする
     */
    function checkGroupEmpty(){
    	$args = func_get_args();
    	$isNotEmpty = true;
    	foreach( $args as $arg ) {
    		if ( !(isset( $this->data[$arg] ) && mb_strlen($this->data[$arg]) > 0)) {
    			$isNotEmpty = false;
    		}
    	}
    	return $isNotEmpty;
    }
	
		/**
     * いずれのフィールドも半角数字で1～4桁で構成されていれば真を返します。
	 * 電話番号を項目分けしている時に使用
     *
     * @return boolean
     */
    function checkGroupZipCodeequ() {
    	$args = func_get_args();
		$zip = "";
    	foreach( $args as $arg ) {
    		if ( isset( $this->data[$arg] ) && mb_strlen($this->data[$arg]) > 0) {
    			$zip .= (String)$this->data[$arg];
    		}
    	}
		if(mb_strlen($zip) != 7){
			return false;
		}
		return true;
    }
}